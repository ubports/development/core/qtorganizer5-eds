/*
 * Copyright 2013 Canonical Ltd.
 * Copyright 2022 Guido Berhoerster <guido+qtorganizer5-eds@berhoerster.name>
 *
 * This file is part of canonical-pim-service.
 *
 * contact-service-app is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * contact-service-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "qorganizer-eds-engine.h"
#include "qorganizer-eds-fetchrequestdata.h"
#include "qorganizer-eds-fetchbyidrequestdata.h"
#include "qorganizer-eds-fetchocurrencedata.h"
#include "qorganizer-eds-saverequestdata.h"
#include "qorganizer-eds-removerequestdata.h"
#include "qorganizer-eds-removebyidrequestdata.h"
#include "qorganizer-eds-savecollectionrequestdata.h"
#include "qorganizer-eds-removecollectionrequestdata.h"
#include "qorganizer-eds-viewwatcher.h"
#include "qorganizer-eds-enginedata.h"
#include "qorganizer-eds-source-registry.h"
#include "qorganizer-eds-parseeventthread.h"

#include <QtCore/qdebug.h>
#include <QtCore/QPointer>
#include <QtCore/QTimeZone>

#include <QtOrganizer/QOrganizerEventAttendee>
#include <QtOrganizer/QOrganizerItemLocation>
#include <QtOrganizer/QOrganizerEventTime>
#include <QtOrganizer/QOrganizerItemFetchRequest>
#include <QtOrganizer/QOrganizerItemFetchByIdRequest>
#include <QtOrganizer/QOrganizerItemSaveRequest>
#include <QtOrganizer/QOrganizerItemRemoveRequest>
#include <QtOrganizer/QOrganizerItemRemoveByIdRequest>
#include <QtOrganizer/QOrganizerCollectionFetchRequest>
#include <QtOrganizer/QOrganizerCollectionSaveRequest>
#include <QtOrganizer/QOrganizerCollectionRemoveRequest>
#include <QtOrganizer/QOrganizerEvent>
#include <QtOrganizer/QOrganizerTodo>
#include <QtOrganizer/QOrganizerTodoTime>
#include <QtOrganizer/QOrganizerJournal>
#include <QtOrganizer/QOrganizerJournalTime>
#include <QtOrganizer/QOrganizerItemIdFetchRequest>
#include <QtOrganizer/QOrganizerItemIdFilter>
#include <QtOrganizer/QOrganizerItemReminder>
#include <QtOrganizer/QOrganizerItemAudibleReminder>
#include <QtOrganizer/QOrganizerItemVisualReminder>
#include <QtOrganizer/QOrganizerEventOccurrence>
#include <QtOrganizer/QOrganizerTodoOccurrence>
#include <QtOrganizer/QOrganizerItemParent>
#include <QtOrganizer/QOrganizerItemExtendedDetail>

#include <glib.h>
#include <libecal/libecal.h>
#include <evolution-data-server-lomiri/e-source-lomiri.h>

using namespace QtOrganizer;
QOrganizerEDSEngineData *QOrganizerEDSEngine::m_globalData = 0;

QOrganizerEDSEngine* QOrganizerEDSEngine::createEDSEngine(const QMap<QString, QString>& parameters)
{
    Q_UNUSED(parameters);

    /* Make sure e_source_get_extension() sees Lomiri extension as
     * ESourceExtension's children type. */
    g_type_ensure(E_TYPE_SOURCE_LOMIRI);

    if (!m_globalData) {
        m_globalData = new QOrganizerEDSEngineData();
        m_globalData->m_sourceRegistry = new SourceRegistry;
    }
    m_globalData->m_refCount.ref();
    return new QOrganizerEDSEngine(m_globalData);
}

QOrganizerEDSEngine::QOrganizerEDSEngine(QOrganizerEDSEngineData *data)
    : d(data)
{
    d->m_sharedEngines << this;

    Q_FOREACH(const QByteArray &sourceId, d->m_sourceRegistry->sourceIds()){
        onSourceAdded(sourceId);
    }
    QObject::connect(d->m_sourceRegistry, &SourceRegistry::sourceAdded,
                     this, &QOrganizerEDSEngine::onSourceAdded);
    QObject::connect(d->m_sourceRegistry, &SourceRegistry::sourceRemoved,
                     this, &QOrganizerEDSEngine::onSourceRemoved);
    QObject::connect(d->m_sourceRegistry, &SourceRegistry::sourceUpdated,
                     this, &QOrganizerEDSEngine::onSourceUpdated);
    d->m_sourceRegistry->load(managerUri());
}

QOrganizerEDSEngine::~QOrganizerEDSEngine()
{
    while(m_runningRequests.count()) {
        QOrganizerAbstractRequest *req = m_runningRequests.keys().first();
        req->cancel();
        QOrganizerEDSEngine::requestDestroyed(req);
    }

    d->m_sharedEngines.remove(this);
    if (!d->m_refCount.deref()) {
        delete d;
        m_globalData = 0;
    }
}

QString QOrganizerEDSEngine::managerName() const
{
    return QStringLiteral("eds");
}

void QOrganizerEDSEngine::itemsAsync(QOrganizerItemFetchRequest *req)
{
    FetchRequestData *data = new FetchRequestData(this,
                                                  d->m_sourceRegistry->sourceIds(),
                                                  req);
    // avoid query if the filter is invalid
    if (data->filterIsValid()) {
        itemsAsyncStart(data);
    } else {
        data->finish();
    }
}

void QOrganizerEDSEngine::itemsAsyncStart(FetchRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    QByteArray sourceId = data->nextSourceId();
    if (!sourceId.isEmpty()) {
        EClient *client = data->parent()->d->m_sourceRegistry->client(sourceId);
        data->setClient(client);
        g_object_unref(client);

        if (data->hasDateInterval()) {
            e_cal_client_generate_instances(data->client(),
                                            data->startDate(),
                                            data->endDate(),
                                            data->cancellable(),
                                            QOrganizerEDSEngine::itemsAsyncListed,
                                            data,
                                            (GDestroyNotify) QOrganizerEDSEngine::itemsAsyncDone);
        } else {
            // if no date interval was set we return only the main events without recurrence
            e_cal_client_get_object_list_as_comps(E_CAL_CLIENT(client),
                                                  data->dateFilter().toUtf8().data(),
                                                  data->cancellable(),
                                                  (GAsyncReadyCallback) QOrganizerEDSEngine::itemsAsyncListedAsComps,
                                                  data);
        }
    } else {
        data->finish();
    }
}

void QOrganizerEDSEngine::itemsAsyncDone(FetchRequestData *data)
{
    if (data->isLive()) {
        data->compileCurrentIds();
        itemsAsyncFetchDeatachedItems(data);
    } else {
        releaseRequestData(data);
    }
}

void QOrganizerEDSEngine::itemsAsyncFetchDeatachedItems(FetchRequestData *data)
{
    QByteArray parentId = data->nextParentId();
    if (!parentId.isEmpty()) {
        e_cal_client_get_objects_for_uid(E_CAL_CLIENT(data->client()),
                                         parentId.data(),
                                         data->cancellable(),
                                         (GAsyncReadyCallback) QOrganizerEDSEngine::itemsAsyncListByIdListed,
                                         data);
    } else {
        itemsAsyncStart(data);
    }
}

void QOrganizerEDSEngine::itemsAsyncListByIdListed(GObject *source,
                                                   GAsyncResult *res,
                                                   FetchRequestData *data)
{
    Q_UNUSED(source);
    GError *gError = 0;
    GSList *events = 0;
    e_cal_client_get_objects_for_uid_finish(E_CAL_CLIENT(data->client()),
                                            res,
                                            &events,
                                            &gError);
    if (gError) {
        qWarning() << "Fail to list deatached events in calendar" << gError->message;
        g_error_free(gError);
        gError = 0;
        if (data->isLive()) {
            data->finish(QOrganizerManager::InvalidCollectionError);
        } else {
            releaseRequestData(data);
        }
        return;
    }

    for(GSList *e = events; e != NULL; e = e->next) {
        ICalComponent * ical = e_cal_component_get_icalcomponent(static_cast<ECalComponent*>(e->data));
        data->appendDeatachedResult(ical);
    }

    itemsAsyncFetchDeatachedItems(data);
}


gboolean QOrganizerEDSEngine::itemsAsyncListed(ICalComponent *icalComp,
                                               ICalTime *instanceStart,
                                               ICalTime *instanceEnd,
                                               gpointer user_data,
                                               GCancellable *cancellable,
                                               GError **error)
{
    Q_UNUSED(instanceStart);
    Q_UNUSED(instanceEnd);
    Q_UNUSED(cancellable);
    Q_UNUSED(error);

    FetchRequestData *data = static_cast<FetchRequestData *>(user_data);
    if (data->isLive()) {
        g_object_ref(icalComp);
        data->appendResult(icalComp);
        return TRUE;
    }
    return FALSE;
}

void QOrganizerEDSEngine::itemsAsyncListedAsComps(GObject *source,
                                                  GAsyncResult *res,
                                                  FetchRequestData *data)
{
    Q_UNUSED(source);
    GError *gError = 0;
    GSList *events = 0;
    e_cal_client_get_object_list_as_comps_finish(E_CAL_CLIENT(data->client()),
                                                 res,
                                                 &events,
                                                 &gError);
    if (gError) {
        qWarning() << "Fail to list events in calendar" << gError->message;
        g_error_free(gError);
        gError = 0;
        if (data->isLive()) {
            data->finish(QOrganizerManager::InvalidCollectionError);
        } else {
            releaseRequestData(data);
        }
        return;
    }

    // check if request was destroyed by the caller
    if (data->isLive()) {
        QOrganizerItemFetchRequest *req = data->request<QOrganizerItemFetchRequest>();
        if (req) {
            data->appendResults(data->parent()->parseEvents(data->sourceId(),
                                                            events,
                                                            false,
                                                            req->fetchHint().detailTypesHint()));
        }
        itemsAsyncStart(data);
    } else {
        releaseRequestData(data);
    }
}

void QOrganizerEDSEngine::itemsByIdAsync(QOrganizerItemFetchByIdRequest *req)
{
    FetchByIdRequestData *data = new FetchByIdRequestData(this, req);
    itemsByIdAsyncStart(data);
}

void QOrganizerEDSEngine::itemsByIdAsyncStart(FetchByIdRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    QOrganizerItemId id = data->nextId();
    if (!id.isNull()) {
        QByteArray collectionId;
        QByteArray fullItemId = idToEds(id, &collectionId);
        QByteArray rId;
        QByteArray itemId = toComponentId(fullItemId, &rId);

        EClient *client = data->parent()->d->m_sourceRegistry->client(collectionId);
        if (client) {
            data->setClient(client);
            e_cal_client_get_object(data->client(),
                                    itemId.data(),
                                    rId.data(),
                                    data->cancellable(),
                                    (GAsyncReadyCallback) QOrganizerEDSEngine::itemsByIdAsyncListed,
                                    data);
            g_object_unref(client);
            return;
        }
    } else if (data->end()) {
        data->finish();
        return;
    }
    qWarning() << "Invalid item id" << id;
    data->appendResult(QOrganizerItem());
    itemsByIdAsyncStart(data);
}

void QOrganizerEDSEngine::itemsByIdAsyncListed(GObject *client,
                                               GAsyncResult *res,
                                               FetchByIdRequestData *data)
{
    Q_UNUSED(client);
    GError *gError = 0;
    ICalComponent *icalComp = 0;
    e_cal_client_get_object_finish(data->client(), res, &icalComp, &gError);
    if (gError) {
        qWarning() << "Fail to list events in calendar" << gError->message;
        g_error_free(gError);
        gError = 0;
        data->appendResult(QOrganizerItem());
    } else if (icalComp && data->isLive()) {
        GSList *events = g_slist_append(0, icalComp);
        QList<QOrganizerItem> items;
        QOrganizerItemFetchByIdRequest *req = data->request<QOrganizerItemFetchByIdRequest>();
        items = data->parent()->parseEvents(data->currentSourceId(),
                                            events,
                                            true,
                                            req->fetchHint().detailTypesHint());
        Q_ASSERT(items.size() == 1);
        data->appendResult(items[0]);
        g_slist_free_full(events, (GDestroyNotify) g_object_unref);
    }

    if (data->isLive()) {
        itemsByIdAsyncStart(data);
    } else {
        releaseRequestData(data);
    }
}

void QOrganizerEDSEngine::itemOcurrenceAsync(QOrganizerItemOccurrenceFetchRequest *req)
{
    FetchOcurrenceData *data = new FetchOcurrenceData(this, req);

    QByteArray rId;
    QByteArray edsItemId = idToEds(req->parentItem().id());
    QByteArray cId = toComponentId(edsItemId, &rId);

    EClient *client = data->parent()->d->m_sourceRegistry->client(req->parentItem().collectionId().localId());
    if (client) {
        data->setClient(client);
        e_cal_client_get_object(data->client(),
                                cId, rId,
                                data->cancellable(),
                                (GAsyncReadyCallback) QOrganizerEDSEngine::itemOcurrenceAsyncGetObjectDone,
                                data);
        g_object_unref(client);
    } else {
        qWarning() << "Fail to find collection:" << req->parentItem().collectionId();
        data->finish(QOrganizerManager::DoesNotExistError);
    }
}

void QOrganizerEDSEngine::itemOcurrenceAsyncGetObjectDone(GObject *source,
                                                          GAsyncResult *res,
                                                          FetchOcurrenceData *data)
{
    Q_UNUSED(source);
    GError *error = 0;
    ICalComponent *comp = 0;
    e_cal_client_get_object_finish(data->client(), res, &comp, &error);
    if (error) {
        qWarning() << "Fail to get object for id:" << data->request<QOrganizerItemOccurrenceFetchRequest>()->parentItem();
        g_error_free(error);
        if (data->isLive()) {
            data->finish(QOrganizerManager::DoesNotExistError);
        } else {
            releaseRequestData(data);
        }
        return;
    }

    if (data->isLive()) {
        e_cal_client_generate_instances_for_object(data->client(),
                                                   comp,
                                                   data->startDate(),
                                                   data->endDate(),
                                                   data->cancellable(),
                                                   QOrganizerEDSEngine::itemOcurrenceAsyncListed,
                                                   data,
                                                   (GDestroyNotify) QOrganizerEDSEngine::itemOcurrenceAsyncDone);
    } else {
        releaseRequestData(data);
    }
}

gboolean QOrganizerEDSEngine::itemOcurrenceAsyncListed(ICalComponent *icalComp,
                                                       ICalTime *instanceStart,
                                                       ICalTime *instanceEnd,
                                                       gpointer user_data,
                                                       GCancellable *cancellable,
                                                       GError **error)
{
    Q_UNUSED(instanceStart);
    Q_UNUSED(instanceEnd);
    Q_UNUSED(cancellable);
    Q_UNUSED(error);

    FetchOcurrenceData *data = static_cast<FetchOcurrenceData *>(user_data);
    // check if request was destroyed by the caller
    if (data->isLive()) {
        g_object_ref(icalComp);
        data->appendResult(icalComp);
    } else {
        releaseRequestData(data);
    }
    return TRUE;
}

void QOrganizerEDSEngine::itemOcurrenceAsyncDone(FetchOcurrenceData *data)
{
    if (data->isLive()) {
        data->finish();
    } else {
        releaseRequestData(data);
    }
}

QList<QOrganizerItem> QOrganizerEDSEngine::items(const QList<QOrganizerItemId> &itemIds,
                                                 const QOrganizerItemFetchHint &fetchHint,
                                                 QMap<int, QOrganizerManager::Error> *errorMap,
                                                 QOrganizerManager::Error *error)
{
    QOrganizerItemFetchByIdRequest *req = new QOrganizerItemFetchByIdRequest(this);
    req->setIds(itemIds);
    req->setFetchHint(fetchHint);

    startRequest(req);
    waitForRequestFinished(req, 0);

    if (error) {
        *error = req->error();
    }

    if (errorMap) {
        *errorMap = req->errorMap();
    }
    req->deleteLater();
    return req->items();
}

QList<QOrganizerItem> QOrganizerEDSEngine::items(const QOrganizerItemFilter &filter,
                                                 const QDateTime &startDateTime,
                                                 const QDateTime &endDateTime,
                                                 int maxCount,
                                                 const QList<QOrganizerItemSortOrder> &sortOrders,
                                                 const QOrganizerItemFetchHint &fetchHint,
                                                 QOrganizerManager::Error *error)
{
    QOrganizerItemFetchRequest *req = new QOrganizerItemFetchRequest(this);

    req->setFilter(filter);
    req->setStartDate(startDateTime);
    req->setEndDate(endDateTime);
    req->setMaxCount(maxCount);
    req->setSorting(sortOrders);
    req->setFetchHint(fetchHint);

    startRequest(req);
    waitForRequestFinished(req, 0);

    if (error) {
        *error = req->error();
    }

    req->deleteLater();
    return req->items();
}

QList<QOrganizerItemId> QOrganizerEDSEngine::itemIds(const QOrganizerItemFilter &filter,
                                                     const QDateTime &startDateTime,
                                                     const QDateTime &endDateTime,
                                                     const QList<QOrganizerItemSortOrder> &sortOrders,
                                                     QOrganizerManager::Error *error)
{
    qWarning() << Q_FUNC_INFO << "Not implemented";
    QList<QOrganizerItemId> items;
    if (error) {
        *error = QOrganizerManager::NotSupportedError;
    }
    return items;
}

QList<QOrganizerItem> QOrganizerEDSEngine::itemOccurrences(const QOrganizerItem &parentItem,
                                                           const QDateTime &startDateTime,
                                                           const QDateTime &endDateTime,
                                                           int maxCount,
                                                           const QOrganizerItemFetchHint &fetchHint,
                                                           QOrganizerManager::Error *error)
{
    QOrganizerItemOccurrenceFetchRequest *req = new QOrganizerItemOccurrenceFetchRequest(this);

    req->setParentItem(parentItem);
    req->setStartDate(startDateTime);
    req->setEndDate(endDateTime);
    req->setMaxOccurrences(maxCount);
    req->setFetchHint(fetchHint);

    startRequest(req);
    waitForRequestFinished(req, 0);

    if (error) {
        *error = req->error();
    }

    req->deleteLater();
    return req->itemOccurrences();
}

QList<QOrganizerItem> QOrganizerEDSEngine::itemsForExport(const QDateTime &startDateTime,
                                                                 const QDateTime &endDateTime,
                                                                 const QOrganizerItemFilter &filter,
                                                                 const QList<QOrganizerItemSortOrder> &sortOrders,
                                                                 const QOrganizerItemFetchHint &fetchHint,
                                                                 QOrganizerManager::Error *error)
{
    qWarning() << Q_FUNC_INFO << "Not implemented";
    if (error) {
        *error = QOrganizerManager::NotSupportedError;
    }
    return QList<QOrganizerItem>();

}

void QOrganizerEDSEngine::saveItemsAsync(QOrganizerItemSaveRequest *req)
{
    if (req->items().count() == 0) {
        QOrganizerManagerEngine::updateItemSaveRequest(req,
                                                       QList<QOrganizerItem>(),
                                                       QOrganizerManager::NoError,
                                                       QMap<int, QOrganizerManager::Error>(),
                                                       QOrganizerAbstractRequest::FinishedState);
        return;
    }
    SaveRequestData *data = new SaveRequestData(this, req);
    saveItemsAsyncStart(data);
}

void QOrganizerEDSEngine::saveItemsAsyncStart(SaveRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    QByteArray sourceId = data->nextSourceId();

    if (sourceId.isNull() && data->end()) {
        data->finish();
        return;
    } else {
        bool createItems = true;
        QList<QOrganizerItem> items = data->takeItemsToCreate();
        if (items.isEmpty()) {
            createItems = false;
            items = data->takeItemsToUpdate();
        }

        if (items.isEmpty()) {
            saveItemsAsyncStart(data);
            return;
        }

        /* We have entered this code path because sourceId is not null;
         * however, it can still be empty: that's because the SaveRequestData
         * class returns an empty (but not null!) sourceId for those items
         * which don't have a collection set, and that therefore should be
         * stored into the default collection.
         * The next "if" condition checks exactly for this situation.
         */
        if (sourceId.isEmpty() && createItems) {
            sourceId = data->parent()->d->m_sourceRegistry->defaultCollection().id().localId();
        }

        EClient *client = data->parent()->d->m_sourceRegistry->client(sourceId);
        if (!client) {
            Q_FOREACH(const QOrganizerItem &i, items) {
                data->appendResult(i, QOrganizerManager::InvalidCollectionError);
            }
            saveItemsAsyncStart(data);
            return;
        }

        Q_ASSERT(client);
        data->setClient(client);
        g_object_unref(client);

        bool hasRecurrence = false;
        GSList *comps = parseItems(data->client(),
                                   items,
                                   &hasRecurrence);
        if (comps) {
            data->setWorkingItems(items);
            if (createItems) {
                e_cal_client_create_objects(data->client(),
                                            comps,
					    E_CAL_OPERATION_FLAG_NONE,
                                            data->cancellable(),
                                            (GAsyncReadyCallback) QOrganizerEDSEngine::saveItemsAsyncCreated,
                                            data);
            } else {
                //WORKAROUND: There is no api to say what kind of update we want in case of update recurrence
                // items (E_CAL_OBJ_MOD_ALL, E_CAL_OBJ_MOD_THIS, E_CAL_OBJ_MOD_THISNADPRIOR, E_CAL_OBJ_MOD_THIS_AND_FUTURE)
                // as temporary solution the user can use "update-mode" property in QOrganizerItemSaveRequest object,
                // if not was specified, we will try to guess based on the event list.
                // If the event list does not cotain any recurrence event we will use E_CAL_OBJ_MOD_ALL
                // If the event list cotains any recurrence event we will use E_CAL_OBJ_MOD_THIS
                // all other cases should be explicitly specified using "update-mode" property
                int updateMode = data->updateMode();
                if (updateMode == -1) {
                    updateMode = hasRecurrence ? E_CAL_OBJ_MOD_THIS : E_CAL_OBJ_MOD_ALL;
                }
                e_cal_client_modify_objects(data->client(),
                                            comps,
                                            static_cast<ECalObjModType>(updateMode),
					    E_CAL_OPERATION_FLAG_NONE,
                                            data->cancellable(),
                                            (GAsyncReadyCallback) QOrganizerEDSEngine::saveItemsAsyncModified,
                                            data);
            }
            g_slist_free_full(comps, (GDestroyNotify) g_object_unref);
        } else {
            qWarning() << "Fail to translate items";
        }
    }
}

void QOrganizerEDSEngine::saveItemsAsyncModified(GObject *source_object,
                                                GAsyncResult *res,
                                                SaveRequestData *data)
{
    Q_UNUSED(source_object);

    GError *gError = 0;
    e_cal_client_modify_objects_finish(E_CAL_CLIENT(data->client()),
                                       res,
                                       &gError);

    if (gError) {
        qWarning() << "Fail to modify items" << gError->message;
        g_error_free(gError);
        gError = 0;
        if (data->isLive()) {
            Q_FOREACH(const QOrganizerItem &i, data->workingItems()) {
                data->appendResult(i, QOrganizerManager::UnspecifiedError);
            }
        }
    } else if (data->isLive()) {
        data->appendResults(data->workingItems());
    }

    if (data->isLive()) {
        saveItemsAsyncStart(data);
    } else {
        releaseRequestData(data);
    }
}

void QOrganizerEDSEngine::saveItemsAsyncCreated(GObject *source_object,
                                                GAsyncResult *res,
                                                SaveRequestData *data)
{
    Q_UNUSED(source_object);

    GError *gError = 0;
    GSList *uids = 0;
    e_cal_client_create_objects_finish(E_CAL_CLIENT(data->client()),
                                       res,
                                       &uids,
                                       &gError);
    if (gError) {
        qWarning() << "Fail to create items:" << (void*) data << gError->message;
        g_error_free(gError);
        gError = 0;

        if (data->isLive()) {
            Q_FOREACH(const QOrganizerItem &i, data->workingItems()) {
                data->appendResult(i, QOrganizerManager::UnspecifiedError);
            }
        }
    } else if (data->isLive()) {
        QByteArray currentSourceId = data->currentSourceId();
        if (currentSourceId.isEmpty()) {
            currentSourceId = data->parent()->defaultCollectionId().localId();
        }
        QList<QOrganizerItem> items = data->workingItems();
        QString managerUri = data->parent()->managerUri();
        for(uint i=0, iMax=g_slist_length(uids); i < iMax; i++) {
            QOrganizerItem &item = items[i];
            QByteArray uid(static_cast<const gchar*>(g_slist_nth_data(uids, i)));

            QOrganizerCollectionId collectionId(managerUri, currentSourceId);

            QString itemGuid =
                uid.contains(':') ? uid.mid(uid.lastIndexOf(':') + 1) : uid;
            QOrganizerItemId itemId = idFromEds(collectionId, uid);
            item.setId(itemId);
            item.setGuid(QString::fromUtf8(itemId.localId()));

            item.setCollectionId(collectionId);
        }
        g_slist_free_full(uids, g_free);
        data->appendResults(items);
    }

    // check if request was destroyed by the caller
    if (data->isLive()) {
        saveItemsAsyncStart(data);
    } else {
        releaseRequestData(data);
    }
}

bool QOrganizerEDSEngine::saveItems(QList<QtOrganizer::QOrganizerItem> *items,
                                    const QList<QtOrganizer::QOrganizerItemDetail::DetailType> &detailMask,
                                    QMap<int, QtOrganizer::QOrganizerManager::Error> *errorMap,
                                    QtOrganizer::QOrganizerManager::Error *error)

{
    QOrganizerItemSaveRequest *req = new QOrganizerItemSaveRequest(this);
    req->setItems(*items);
    req->setDetailMask(detailMask);

    startRequest(req);
    waitForRequestFinished(req, 0);

    *errorMap = req->errorMap();
    *error = req->error();
    *items = req->items();

    return (*error == QOrganizerManager::NoError);
}

void QOrganizerEDSEngine::removeItemsByIdAsync(QOrganizerItemRemoveByIdRequest *req)
{
    if (req->itemIds().count() == 0) {
        QOrganizerManagerEngine::updateItemRemoveByIdRequest(req,
                                                             QOrganizerManager::NoError,
                                                             QMap<int, QOrganizerManager::Error>(),
                                                             QOrganizerAbstractRequest::FinishedState);
        return;
    }

    RemoveByIdRequestData *data = new RemoveByIdRequestData(this, req);
    removeItemsByIdAsyncStart(data);
}

void QOrganizerEDSEngine::removeItemsByIdAsyncStart(RemoveByIdRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    QByteArray collectionId = data->next();
    for(; !collectionId.isNull(); collectionId = data->next()) {
        EClient *client = data->parent()->d->m_sourceRegistry->client(collectionId);
        data->setClient(client);
        g_object_unref(client);
        GSList *ids = data->compIds();
        GError *gError = 0;
        e_cal_client_remove_objects_sync(data->client(), ids, E_CAL_OBJ_MOD_THIS, E_CAL_OPERATION_FLAG_NONE, 0, 0);
        if (gError) {
            qWarning() << "Fail to remove Items" << gError->message;
            g_error_free(gError);
            gError = 0;
        }
        data->commit();
    }
    data->finish();
}

void QOrganizerEDSEngine::removeItemsAsync(QOrganizerItemRemoveRequest *req)
{
    if (req->items().count() == 0) {
        QOrganizerManagerEngine::updateItemRemoveRequest(req,
                                                         QOrganizerManager::NoError,
                                                         QMap<int, QOrganizerManager::Error>(),
                                                         QOrganizerAbstractRequest::FinishedState);
        return;
    }

    RemoveRequestData *data = new RemoveRequestData(this, req);
    removeItemsAsyncStart(data);
}

void QOrganizerEDSEngine::removeItemsAsyncStart(RemoveRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    QOrganizerCollectionId collection = data->next();
    for(; !collection.isNull(); collection = data->next()) {
        EClient *client = data->parent()->d->m_sourceRegistry->client(collection.localId());
        Q_ASSERT(client);
        data->setClient(client);
        g_object_unref(client);
        GSList *ids = data->compIds();
        GError *gError = 0;
        e_cal_client_remove_objects_sync(data->client(), ids, E_CAL_OBJ_MOD_THIS, E_CAL_OPERATION_FLAG_NONE, 0, 0);
        if (gError) {
            qWarning() << "Fail to remove Items" << gError->message;
            g_error_free(gError);
            gError = 0;
        }
        data->commit();
    }
    data->finish();
}

bool QOrganizerEDSEngine::removeItems(const QList<QOrganizerItemId> &itemIds,
                                      QMap<int, QOrganizerManager::Error> *errorMap,
                                      QOrganizerManager::Error *error)
{
    QOrganizerItemRemoveByIdRequest *req = new QOrganizerItemRemoveByIdRequest(this);
    req->setItemIds(itemIds);
    startRequest(req);
    waitForRequestFinished(req, 0);

    if (errorMap) {
        *errorMap = req->errorMap();
    }
    if (error) {
        *error = req->error();
    }

    return (*error == QOrganizerManager::NoError);
}

QOrganizerCollectionId QOrganizerEDSEngine::defaultCollectionId() const
{
    return d->m_sourceRegistry->defaultCollection().id();
}

QOrganizerCollection QOrganizerEDSEngine::collection(const QOrganizerCollectionId& collectionId,
                                                     QOrganizerManager::Error* error) const
{
    QOrganizerCollection collection = d->m_sourceRegistry->collection(collectionId.localId());
    if (collection.id().isNull() && error) {
        *error = QOrganizerManager::DoesNotExistError;
    }

    return collection;
}

QList<QOrganizerCollection> QOrganizerEDSEngine::collections(QOrganizerManager::Error* error) const
{
    return const_cast<QOrganizerEDSEngine*>(this)->collections(error);
}

QList<QOrganizerCollection> QOrganizerEDSEngine::collections(QOrganizerManager::Error* error)
{
    QOrganizerCollectionFetchRequest *req = new QOrganizerCollectionFetchRequest(this);

    startRequest(req);
    waitForRequestFinished(req, 0);

    if (error) {
        *error = req->error();
    }

    if (req->error() == QOrganizerManager::NoError) {
        return req->collections();
    } else {
        return QList<QOrganizerCollection>();
    }
}

bool QOrganizerEDSEngine::saveCollection(QOrganizerCollection* collection, QOrganizerManager::Error* error)
{
    QOrganizerCollectionSaveRequest *req = new QOrganizerCollectionSaveRequest(this);
    req->setCollection(*collection);

    startRequest(req);
    waitForRequestFinished(req, 0);

    *error = req->error();
    if ((*error == QOrganizerManager::NoError) &&
        (req->collections().count())) {
        *collection = req->collections()[0];
        return true;
    } else {
        return false;
    }
}

void QOrganizerEDSEngine::saveCollectionAsync(QOrganizerCollectionSaveRequest *req)
{
    if (req->collections().count() == 0) {
        QOrganizerManagerEngine::updateCollectionSaveRequest(req,
                                                             QList<QOrganizerCollection>(),
                                                             QOrganizerManager::NoError,
                                                             QMap<int, QOrganizerManager::Error>(),
                                                             QOrganizerAbstractRequest::FinishedState);
        return;
    }

    ESourceRegistry *registry = d->m_sourceRegistry->object();
    SaveCollectionRequestData *requestData = new SaveCollectionRequestData(this, req);
    requestData->setRegistry(registry);

    if (requestData->prepareToCreate()) {
        e_source_registry_create_sources(registry,
                                         requestData->sourcesToCreate(),
                                         requestData->cancellable(),
                                         (GAsyncReadyCallback) QOrganizerEDSEngine::saveCollectionAsyncCommited,
                                         requestData);
    } else {
        requestData->prepareToUpdate();
        g_idle_add((GSourceFunc) saveCollectionUpdateAsyncStart, requestData);
    }
}

void QOrganizerEDSEngine::saveCollectionAsyncCommited(ESourceRegistry *registry,
                                                      GAsyncResult *res,
                                                      SaveCollectionRequestData *data)
{
    GError *gError = 0;
    e_source_registry_create_sources_finish(registry, res, &gError);
    if (gError) {
        qWarning() << "Fail to create sources:" << gError->message;
        g_error_free(gError);
        if (data->isLive()) {
            data->finish(QOrganizerManager::InvalidCollectionError);
            return;
        }
    } else if (data->isLive()) {
        data->prepareToUpdate();
        g_idle_add((GSourceFunc) saveCollectionUpdateAsyncStart, data);
    }
}


gboolean QOrganizerEDSEngine::saveCollectionUpdateAsyncStart(SaveCollectionRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return FALSE;
    }

    ESource *source = data->nextSourceToUpdate();
    if (source) {
        e_source_write(source,
                       data->cancellable(),
                       (GAsyncReadyCallback) QOrganizerEDSEngine::saveCollectionUpdateAsynCommited,
                       data);
    } else {
        data->finish();
    }
    return FALSE;
}

void QOrganizerEDSEngine::saveCollectionUpdateAsynCommited(ESource *source,
                                                           GAsyncResult *res,
                                                           SaveCollectionRequestData *data)
{
    GError *gError = 0;

    e_source_write_finish(source, res, &gError);
    if (gError) {
        qWarning() << "Fail to update collection" << gError->message;
        g_error_free(gError);
        if (data->isLive()) {
            data->commitSourceUpdated(source, QOrganizerManager::InvalidCollectionError);
        }
    } else if (data->isLive()) {
        data->commitSourceUpdated(source);
    }

    if (data->isLive()) {
        g_idle_add((GSourceFunc) saveCollectionUpdateAsyncStart, data);
    } else {
        releaseRequestData(data);
    }
}

bool QOrganizerEDSEngine::removeCollection(const QOrganizerCollectionId& collectionId, QOrganizerManager::Error* error)
{
    QOrganizerCollectionRemoveRequest *req = new QOrganizerCollectionRemoveRequest(this);
    req->setCollectionId(collectionId);

    startRequest(req);
    waitForRequestFinished(req, 0);

    if (error) {
        *error = req->error();
    }

    return(req->error() == QOrganizerManager::NoError);
}

void QOrganizerEDSEngine::removeCollectionAsync(QtOrganizer::QOrganizerCollectionRemoveRequest *req)
{
    if (req->collectionIds().count() == 0) {
        QOrganizerManagerEngine::updateCollectionRemoveRequest(req,
                                                             QOrganizerManager::NoError,
                                                             QMap<int, QOrganizerManager::Error>(),
                                                             QOrganizerAbstractRequest::FinishedState);
        return;
    }

    RemoveCollectionRequestData *requestData = new RemoveCollectionRequestData(this, req);
    removeCollectionAsyncStart(0, 0, requestData);
}

void QOrganizerEDSEngine::removeCollectionAsyncStart(GObject *sourceObject,
                                                     GAsyncResult *res,
                                                     RemoveCollectionRequestData *data)
{
    // check if request was destroyed by the caller
    if (!data->isLive()) {
        releaseRequestData(data);
        return;
    }

    if (sourceObject && res) {
        GError *gError = 0;
        if (data->remoteDeletable()) {
            e_source_remote_delete_finish(E_SOURCE(sourceObject), res, &gError);
        } else {
            e_source_remove_finish(E_SOURCE(sourceObject), res, &gError);
        }
        if (gError) {
            qWarning() << "Fail to remove collection" << gError->message;
            g_error_free(gError);
            data->commit(QOrganizerManager::InvalidCollectionError);
        } else {
            data->commit();
        }
    }

    ESource *source = data->begin();
    if (source) {
        ESourceRegistry *registry = NULL;
        gboolean accountRemovable = e_source_get_removable(source);
        gboolean remoteDeletable = e_source_get_remote_deletable(source);

        if ((accountRemovable == FALSE) && (remoteDeletable == FALSE)) {
            qWarning() << "Account not removable will refetch source";
            // WORKAROUND: Sometimes EDS take longer to make a account removable with this we
            // force EDS to update sources infomation
            registry  = e_source_registry_new_sync(NULL, NULL);
            source = e_source_registry_ref_source(registry, e_source_get_uid(source));
            accountRemovable = e_source_get_removable(source);
            remoteDeletable = e_source_get_remote_deletable(source);
        }

        if (remoteDeletable == TRUE) {
            data->setRemoteDeletable(true);
            e_source_remote_delete(source, data->cancellable(),
                                   (GAsyncReadyCallback) QOrganizerEDSEngine::removeCollectionAsyncStart,
                                   data);
        } else if (accountRemovable == TRUE) {
            e_source_remove(source, data->cancellable(),
                            (GAsyncReadyCallback) QOrganizerEDSEngine::removeCollectionAsyncStart,
                            data);
        } else {
            qWarning() << "Source not removable" << e_source_get_uid(source);
            data->commit(QOrganizerManager::InvalidCollectionError);
            removeCollectionAsyncStart(0, 0, data);
        }

        if (registry) {
            g_object_unref(source);
            g_object_unref(registry);
        }
    } else {
        data->finish();
    }

}

void QOrganizerEDSEngine::releaseRequestData(RequestData *data)
{
    data->deleteLater();
}

void QOrganizerEDSEngine::requestDestroyed(QOrganizerAbstractRequest* req)
{
    RequestData *data = m_runningRequests.take(req);
    if (data) {
        data->cancel();
    }
}

bool QOrganizerEDSEngine::startRequest(QOrganizerAbstractRequest* req)
{
    if (!req)
        return false;

    switch (req->type())
    {
        case QOrganizerAbstractRequest::ItemFetchRequest:
            itemsAsync(qobject_cast<QOrganizerItemFetchRequest*>(req));
            break;
        case QOrganizerAbstractRequest::ItemFetchByIdRequest:
            itemsByIdAsync(qobject_cast<QOrganizerItemFetchByIdRequest*>(req));
            break;
        case QOrganizerAbstractRequest::ItemOccurrenceFetchRequest:
            itemOcurrenceAsync(qobject_cast<QOrganizerItemOccurrenceFetchRequest*>(req));
            break;
        case QOrganizerAbstractRequest::CollectionFetchRequest:
            QOrganizerManagerEngine::updateCollectionFetchRequest(qobject_cast<QOrganizerCollectionFetchRequest*>(req),
                                                                  d->m_sourceRegistry->collections(),
                                                                  QOrganizerManager::NoError,
                                                                  QOrganizerAbstractRequest::FinishedState);
            break;
        case QOrganizerAbstractRequest::ItemSaveRequest:
            saveItemsAsync(qobject_cast<QOrganizerItemSaveRequest*>(req));
            break;
        case QOrganizerAbstractRequest::ItemRemoveRequest:
            removeItemsAsync(qobject_cast<QOrganizerItemRemoveRequest*>(req));
            break;
        case QOrganizerAbstractRequest::ItemRemoveByIdRequest:
            removeItemsByIdAsync(qobject_cast<QOrganizerItemRemoveByIdRequest*>(req));
            break;
        case QOrganizerAbstractRequest::CollectionSaveRequest:
            saveCollectionAsync(qobject_cast<QOrganizerCollectionSaveRequest*>(req));
            break;
        case QOrganizerAbstractRequest::CollectionRemoveRequest:
            removeCollectionAsync(qobject_cast<QOrganizerCollectionRemoveRequest*>(req));
            break;
        default:
            updateRequestState(req, QOrganizerAbstractRequest::FinishedState);
            qWarning() << "No implemented request" << req->type();
            break;
    }

    return true;
}

bool QOrganizerEDSEngine::cancelRequest(QOrganizerAbstractRequest* req)
{
    RequestData *data = m_runningRequests.value(req);
    if (data) {
        data->cancel();
        return true;
    }
    qWarning() << "Request is not running" << (void*) req;
    return false;
}

bool QOrganizerEDSEngine::waitForRequestFinished(QOrganizerAbstractRequest* req, int msecs)
{
    Q_ASSERT(req);

    RequestData *data = m_runningRequests.value(req);
    if (data) {
        data->wait(msecs);
        // We can delete the operation already finished
        data->deleteLater();
    }

    return true;
}

QList<QOrganizerItemDetail::DetailType> QOrganizerEDSEngine::supportedItemDetails(QOrganizerItemType::ItemType itemType) const
{
    QList<QOrganizerItemDetail::DetailType> supportedDetails;
    supportedDetails << QOrganizerItemDetail::TypeItemType
                     << QOrganizerItemDetail::TypeGuid
                     << QOrganizerItemDetail::TypeTimestamp
                     << QOrganizerItemDetail::TypeDisplayLabel
                     << QOrganizerItemDetail::TypeDescription
                     << QOrganizerItemDetail::TypeComment
                     << QOrganizerItemDetail::TypeTag
                     << QOrganizerItemDetail::TypeClassification
                     << QOrganizerItemDetail::TypeExtendedDetail;

    if (itemType == QOrganizerItemType::TypeEvent) {
        supportedDetails << QOrganizerItemDetail::TypeRecurrence
                         << QOrganizerItemDetail::TypeEventTime
                         << QOrganizerItemDetail::TypePriority
                         << QOrganizerItemDetail::TypeLocation
                         << QOrganizerItemDetail::TypeReminder
                         << QOrganizerItemDetail::TypeAudibleReminder
                         << QOrganizerItemDetail::TypeEmailReminder
                         << QOrganizerItemDetail::TypeVisualReminder;
    } else if (itemType == QOrganizerItemType::TypeTodo) {
        supportedDetails << QOrganizerItemDetail::TypeRecurrence
                         << QOrganizerItemDetail::TypeTodoTime
                         << QOrganizerItemDetail::TypePriority
                         << QOrganizerItemDetail::TypeTodoProgress
                         << QOrganizerItemDetail::TypeReminder
                         << QOrganizerItemDetail::TypeAudibleReminder
                         << QOrganizerItemDetail::TypeEmailReminder
                         << QOrganizerItemDetail::TypeVisualReminder;
    } else if (itemType == QOrganizerItemType::TypeEventOccurrence) {
        supportedDetails << QOrganizerItemDetail::TypeParent
                         << QOrganizerItemDetail::TypeEventTime
                         << QOrganizerItemDetail::TypePriority
                         << QOrganizerItemDetail::TypeLocation
                         << QOrganizerItemDetail::TypeReminder
                         << QOrganizerItemDetail::TypeAudibleReminder
                         << QOrganizerItemDetail::TypeEmailReminder
                         << QOrganizerItemDetail::TypeVisualReminder;
    } else if (itemType == QOrganizerItemType::TypeTodoOccurrence) {
        supportedDetails << QOrganizerItemDetail::TypeParent
                         << QOrganizerItemDetail::TypeTodoTime
                         << QOrganizerItemDetail::TypePriority
                         << QOrganizerItemDetail::TypeTodoProgress
                         << QOrganizerItemDetail::TypeReminder
                         << QOrganizerItemDetail::TypeAudibleReminder
                         << QOrganizerItemDetail::TypeEmailReminder
                         << QOrganizerItemDetail::TypeVisualReminder;
    } else if (itemType == QOrganizerItemType::TypeJournal) {
        supportedDetails << QOrganizerItemDetail::TypeJournalTime;
    } else if (itemType == QOrganizerItemType::TypeNote) {
        // nothing ;)
    } else {
        supportedDetails.clear();
    }

    return supportedDetails;
}

QList<QOrganizerItemFilter::FilterType> QOrganizerEDSEngine::supportedFilters() const
{
    QList<QOrganizerItemFilter::FilterType> supported;

    supported << QOrganizerItemFilter::InvalidFilter
              << QOrganizerItemFilter::DetailFilter
              << QOrganizerItemFilter::DetailFieldFilter
              << QOrganizerItemFilter::DetailRangeFilter
              << QOrganizerItemFilter::IntersectionFilter
              << QOrganizerItemFilter::UnionFilter
              << QOrganizerItemFilter::IdFilter
              << QOrganizerItemFilter::CollectionFilter
              << QOrganizerItemFilter::DefaultFilter;

    return supported;
}

QList<QOrganizerItemType::ItemType> QOrganizerEDSEngine::supportedItemTypes() const
{
    return QList<QOrganizerItemType::ItemType>() << QOrganizerItemType::TypeEvent
                         << QOrganizerItemType::TypeEventOccurrence
                         << QOrganizerItemType::TypeJournal
                         << QOrganizerItemType::TypeNote
                         << QOrganizerItemType::TypeTodo
                         << QOrganizerItemType::TypeTodoOccurrence;
}

int QOrganizerEDSEngine::runningRequestCount() const
{
    return m_runningRequests.count();
}

void QOrganizerEDSEngine::onSourceAdded(const QByteArray &sourceId)
{
    QOrganizerCollectionId id(managerUri(), sourceId);
    d->watch(id);

    Q_EMIT collectionsAdded(QList<QOrganizerCollectionId>() << id);

    QList<QPair<QOrganizerCollectionId, QOrganizerManager::Operation> > ops;
    ops << qMakePair(id, QOrganizerManager::Add);
    Q_EMIT collectionsModified(ops);
}

void QOrganizerEDSEngine::onSourceRemoved(const QByteArray &sourceId)
{
    d->unWatch(sourceId);
    QOrganizerCollectionId id(managerUri(), sourceId);

    Q_EMIT collectionsRemoved(QList<QOrganizerCollectionId>() << id);

    QList<QPair<QOrganizerCollectionId, QOrganizerManager::Operation> > ops;
    ops << qMakePair(id, QOrganizerManager::Remove);
    Q_EMIT collectionsModified(ops);
}

void QOrganizerEDSEngine::onSourceUpdated(const QByteArray &sourceId)
{
    QOrganizerCollectionId id(managerUri(), sourceId);
    Q_EMIT collectionsChanged(QList<QOrganizerCollectionId>() << id);

    QList<QPair<QOrganizerCollectionId, QOrganizerManager::Operation> > ops;
    ops << qMakePair(id, QOrganizerManager::Change);
    Q_EMIT collectionsModified(ops);
}

QTimeZone QOrganizerEDSEngine::tzFromIcalTime(ICalTime *time, const char *tzId) {
    QByteArray tzLocationName;
    const char* identifier = tzId;
    // handle the TZID /freeassociation.sourceforge.net/[Tzfile]/[Location] case
    const char* pch;
    const char* key = "/freeassociation.sourceforge.net/";
    if ((pch = strstr(identifier, key)))
    {
        identifier = pch + strlen(key);
        key = "Tzfile/"; // some don't have this, so check for it separately
        if ((pch = strstr(identifier, key)))
            identifier = pch + strlen(key);
    }

    if (QTimeZone::isTimeZoneIdAvailable(QByteArray(identifier))) {
        tzLocationName = QByteArray(identifier);
    } else if (i_cal_time_is_utc(time)) {
        tzLocationName = "UTC";
    } else {
        tzLocationName = m_globalData->timeZoneFromCity(QByteArray(identifier));
    }
    qDebug() << "tzFromIcalTime:" << identifier << "tz:" << tzLocationName;
    if (tzLocationName.isEmpty()) {
        return QTimeZone();
    } else {
        return QTimeZone(tzLocationName);
    }
}

QDateTime QOrganizerEDSEngine::fromIcalTime(ECalClient *client, ICalTime *time, const char *tzId)
{
    uint tmTime;
    bool allDayEvent = i_cal_time_is_date(time);

    // check if icaltimetype contains a time and timezone
    if (!allDayEvent && tzId) {

        QTimeZone qTz = tzFromIcalTime(time, tzId);
        if (!qTz.isValid()) {
            const char* identifier {};
            ICalTimezone *timezone = i_cal_timezone_get_builtin_timezone_from_tzid(tzId);

            if (!timezone) { // fallback
                timezone = i_cal_timezone_get_builtin_timezone(tzId);
            }

            if (client && (!timezone)) { // ok we have a strange tzid... ask EDS to look it up in VTIMEZONES
                if (!e_cal_client_get_timezone_sync(client, tzId, &timezone, nullptr, nullptr)) {
                    qDebug() << "No way, even e_cal_client_get_timezone_sync doesn't work!";
                    timezone = nullptr;
                }
            }

            if (timezone != nullptr) {
                identifier = i_cal_timezone_get_display_name(timezone);

                // try with location
                if (identifier == nullptr) {
                    identifier = i_cal_timezone_get_location(timezone);
                }
            }

            if (identifier != nullptr) {
                qTz = tzFromIcalTime(time, identifier);
            }

            if (!qTz.isValid()) {
                qWarning() << " unable to find Timezone with tzID" << identifier << " fall back to local time";
                qTz = QTimeZone(Qt::LocalTime);
            }
        }

        QDateTime dt(
            QDate(i_cal_time_get_year(time), i_cal_time_get_month(time), i_cal_time_get_day(time)),
            QTime(i_cal_time_get_hour(time), i_cal_time_get_minute(time), i_cal_time_get_second(time)),
            qTz);

        return dt;
    } else {
        tmTime = i_cal_time_as_timet(time);
        QDateTime t = QDateTime::fromTime_t(tmTime, Qt::UTC);
        // all day events will set as local time
        // floating time events will be set as UTC
        QDateTime tt;
        if (allDayEvent)
          tt = QDateTime(t.date(), QTime(0,0,0),
                         QTimeZone(QTimeZone::systemTimeZoneId()));
        else
          tt = QDateTime(t.date(), t.time(), Qt::UTC);
        return tt;
    }
}

ICalTime* QOrganizerEDSEngine::fromQDateTime(const QDateTime &dateTime,
                                                bool allDay,
                                                QByteArray *tzId)
{
    QDateTime finalDate(dateTime);
    QTimeZone tz;

    if (!allDay) {
        switch (finalDate.timeSpec()) {
        case Qt::UTC:
        case Qt::OffsetFromUTC:
            // convert date to UTC timezone
            tz = QTimeZone("UTC");
            finalDate = finalDate.toTimeZone(tz);
            break;
        case Qt::TimeZone:
            tz = finalDate.timeZone();
            if (!tz.isValid()) {
                // floating time
                finalDate = QDateTime(finalDate.date(), finalDate.time(), Qt::UTC);
            }
            break;
        case Qt::LocalTime:
            tz = QTimeZone(QTimeZone::systemTimeZoneId());
            finalDate = finalDate.toTimeZone(tz);
            break;
        default:
            break;
        }
    }

    if (tz.isValid()) {
        ICalTimezone *timezone = 0;
        timezone = i_cal_timezone_get_builtin_timezone(tz.id().constData());
        *tzId = QByteArray(i_cal_timezone_get_tzid(timezone));
        return i_cal_time_new_from_timet_with_zone(finalDate.toTime_t(), allDay, timezone);
    } else {
        bool invalidTime = allDay || !finalDate.time().isValid();
        finalDate = QDateTime(finalDate.date(),
                              invalidTime ? QTime(0, 0, 0) : finalDate.time(),
                              Qt::UTC);

        *tzId = "";
        return i_cal_time_new_from_timet_with_zone(finalDate.toTime_t(), allDay, NULL);
    }
}

void QOrganizerEDSEngine::parseStartTime(ECalClient *client, ECalComponent *comp, QOrganizerItem *item)
{
    ECalComponentDateTime *start = e_cal_component_get_dtstart(comp);
    ICalTime *date = start ? e_cal_component_datetime_get_value(start) : NULL;
    if (date) {
        QOrganizerEventTime etr = item->detail(QOrganizerItemDetail::TypeEventTime);
        QDateTime qdtime = fromIcalTime(client, date, e_cal_component_datetime_get_tzid(start));
        if (qdtime.isValid()) {
            etr.setStartDateTime(qdtime);
        }
        if (i_cal_time_is_date(date) != etr.isAllDay()) {
            etr.setAllDay(i_cal_time_is_date(date));
        }
        item->saveDetail(&etr);
    }
    e_cal_component_datetime_free(start);
}

void QOrganizerEDSEngine::parseTodoStartTime(ECalClient *client, ECalComponent *comp, QOrganizerItem *item)
{
    ECalComponentDateTime *start = e_cal_component_get_dtstart(comp);
    ICalTime *date = start ? e_cal_component_datetime_get_value(start) : NULL;
    if (date) {
        QOrganizerTodoTime etr = item->detail(QOrganizerItemDetail::TypeTodoTime);
        QDateTime qdtime = fromIcalTime(client, date, e_cal_component_datetime_get_tzid(start));
        if (qdtime.isValid()) {
            etr.setStartDateTime(qdtime);
        }
        if (i_cal_time_is_date(date) != etr.isAllDay()) {
            etr.setAllDay(i_cal_time_is_date(date));
        }
        item->saveDetail(&etr);
    }
    e_cal_component_datetime_free(start);
}

void QOrganizerEDSEngine::parseEndTime(ECalClient *client, ECalComponent *comp, QOrganizerItem *item)
{
    ECalComponentDateTime *end = e_cal_component_get_dtend(comp);
    ICalTime *date = end ? e_cal_component_datetime_get_value(end) : NULL;
    if (date) {
        QOrganizerEventTime etr = item->detail(QOrganizerItemDetail::TypeEventTime);
        QDateTime qdtime = fromIcalTime(client, date, e_cal_component_datetime_get_tzid(end));
        if (qdtime.isValid()) {
            etr.setEndDateTime(qdtime);
        }
        if (i_cal_time_is_date(date) != etr.isAllDay()) {
            etr.setAllDay(i_cal_time_is_date(date));
        }
        item->saveDetail(&etr);
    }
    e_cal_component_datetime_free(end);
}

void QOrganizerEDSEngine::parseWeekRecurrence(ICalRecurrence *rule, QtOrganizer::QOrganizerRecurrenceRule *qRule)
{
    static QMap<ICalRecurrenceWeekday, Qt::DayOfWeek>  daysOfWeekMap({
        { I_CAL_MONDAY_WEEKDAY, Qt::Monday },
        { I_CAL_THURSDAY_WEEKDAY, Qt::Thursday },
        { I_CAL_WEDNESDAY_WEEKDAY, Qt::Wednesday },
        { I_CAL_TUESDAY_WEEKDAY, Qt::Tuesday },
        { I_CAL_FRIDAY_WEEKDAY, Qt::Friday },
        { I_CAL_SATURDAY_WEEKDAY, Qt::Saturday },
        { I_CAL_SUNDAY_WEEKDAY, Qt::Sunday }
    });

    qRule->setFrequency(QOrganizerRecurrenceRule::Weekly);

    QSet<Qt::DayOfWeek> daysOfWeek;
    for (int d=0; d <= Qt::Sunday; d++) {
        short day = i_cal_recurrence_get_by_day(rule, d);
        if (day != I_CAL_RECURRENCE_ARRAY_MAX) {
            daysOfWeek.insert(daysOfWeekMap[i_cal_recurrence_day_day_of_week(day)]);
        }
    }

    qRule->setDaysOfWeek(daysOfWeek);
}

void QOrganizerEDSEngine::parseMonthRecurrence(ICalRecurrence *rule, QtOrganizer::QOrganizerRecurrenceRule *qRule)
{
    qRule->setFrequency(QOrganizerRecurrenceRule::Monthly);

    QSet<int> daysOfMonth;
    for (int d=0; d < I_CAL_BY_MONTHDAY_SIZE; d++) {
        short day = i_cal_recurrence_get_by_month_day(rule, d);
        if (day != I_CAL_RECURRENCE_ARRAY_MAX) {
            daysOfMonth.insert(day);
        }
    }
    qRule->setDaysOfMonth(daysOfMonth);
}

void QOrganizerEDSEngine::parseYearRecurrence(ICalRecurrence *rule, QtOrganizer::QOrganizerRecurrenceRule *qRule)
{
    qRule->setFrequency(QOrganizerRecurrenceRule::Yearly);

    QSet<int> daysOfYear;
    for (int d=0; d < I_CAL_BY_YEARDAY_SIZE; d++) {
        short day = i_cal_recurrence_get_by_year_day(rule, d);
        if (day != I_CAL_RECURRENCE_ARRAY_MAX) {
            daysOfYear.insert(day);
        }
    }
    qRule->setDaysOfYear(daysOfYear);

    QSet<QOrganizerRecurrenceRule::Month> monthOfYear;
    for (int d=0; d < I_CAL_BY_MONTH_SIZE; d++) {
        short month = i_cal_recurrence_get_by_month(rule, d);
        if (month != I_CAL_RECURRENCE_ARRAY_MAX) {
            monthOfYear.insert(static_cast<QOrganizerRecurrenceRule::Month>(month));
        }
    }
    qRule->setMonthsOfYear(monthOfYear);
}

void QOrganizerEDSEngine::parseRecurrence(ECalClient *client, ECalComponent *comp, QOrganizerItem *item)
{
    // recurence
    if (e_cal_component_has_rdates(comp)) {
        QSet<QDate> dates;
        GSList *periodList = e_cal_component_get_rdates(comp);
        for(GSList *i = periodList; i != 0; i = i->next) {
            ECalComponentPeriod *period = (ECalComponentPeriod*) i->data;
            //TODO: get timezone info
            QDateTime dt = fromIcalTime(client, e_cal_component_period_get_start(period), 0);
            if (dt.isValid())
                dates.insert(dt.date());
            //TODO: period.end, period.duration
        }
        g_slist_free_full(periodList, e_cal_component_period_free);

        QOrganizerItemRecurrence rec = item->detail(QOrganizerItemDetail::TypeRecurrence);
        rec.setRecurrenceDates(dates);
        item->saveDetail(&rec);
    }

    if (e_cal_component_has_exdates(comp)) {
        QSet<QDate> dates;
        GSList *exdateList = e_cal_component_get_exdates(comp);
        for(GSList *i = exdateList; i != 0; i = i->next) {
            ECalComponentDateTime* dateTime = (ECalComponentDateTime*) i->data;
            QDateTime dt = fromIcalTime(client, e_cal_component_datetime_get_value(dateTime),
                                        e_cal_component_datetime_get_tzid(dateTime));
            if (dt.isValid())
                dates.insert(dt.date());
        }
        g_slist_free_full(exdateList, e_cal_component_datetime_free);

        QOrganizerItemRecurrence irec = item->detail(QOrganizerItemDetail::TypeRecurrence);
        irec.setExceptionDates(dates);
        item->saveDetail(&irec);
    }

    // rules
    GSList *ruleList = e_cal_component_get_rrules(comp);
    if (ruleList) {
        QSet<QOrganizerRecurrenceRule> qRules;

        for(GSList *i = ruleList; i != 0; i = i->next) {
            ICalRecurrence *rule = (ICalRecurrence*) i->data;
            QOrganizerRecurrenceRule qRule;
            switch (i_cal_recurrence_get_freq(rule)) {
                case I_CAL_SECONDLY_RECURRENCE:
                case I_CAL_MINUTELY_RECURRENCE:
                case I_CAL_HOURLY_RECURRENCE:
                    qWarning() << "Recurrence frequency not supported";
                    break;
                case I_CAL_DAILY_RECURRENCE:
                    qRule.setFrequency(QOrganizerRecurrenceRule::Daily);
                    break;
                case I_CAL_WEEKLY_RECURRENCE:
                    parseWeekRecurrence(rule, &qRule);
                    break;
                case I_CAL_MONTHLY_RECURRENCE:
                    parseMonthRecurrence(rule, &qRule);
                    break;
                case I_CAL_YEARLY_RECURRENCE:
                    parseYearRecurrence(rule, &qRule);
                    break;
                case I_CAL_NO_RECURRENCE:
                    break;
            }

            ICalTime *until = i_cal_recurrence_get_until(rule);
            if (i_cal_time_is_date(until)) {
                char *dateStr = i_cal_time_as_ical_string(until);
                QDate dt = QDate::fromString(dateStr, "yyyyMMdd");
                if (dt.isValid()) {
                    qRule.setLimit(dt);
                }
                g_free(dateStr);
            } else if (i_cal_recurrence_get_count(rule) > 0) {
                qRule.setLimit(i_cal_recurrence_get_count(rule));
            }
            g_object_unref(until);

            qRule.setInterval(i_cal_recurrence_get_interval(rule));

            QSet<int> positions;
            for (int d=0; d < I_CAL_BY_SETPOS_SIZE; d++) {
                short day = i_cal_recurrence_get_by_set_pos(rule, d);
                if (day != I_CAL_RECURRENCE_ARRAY_MAX) {
                    positions.insert(day);
                }
            }
            qRule.setPositions(positions);

            qRules << qRule;
        }

        if (!qRules.isEmpty()) {
            QOrganizerItemRecurrence irec = item->detail(QOrganizerItemDetail::TypeRecurrence);
            irec.setRecurrenceRules(qRules);
            item->saveDetail(&irec);
        }

        g_slist_free_full(ruleList, g_object_unref);
    }
    // TODO: exeptions rules
}

void QOrganizerEDSEngine::parsePriority(ECalComponent *comp, QOrganizerItem *item)
{
    gint priority = e_cal_component_get_priority(comp);
    if (priority) {
        QOrganizerItemPriority iPriority = item->detail(QOrganizerItemDetail::TypePriority);
        if ((priority >= QOrganizerItemPriority::UnknownPriority) &&
            (priority <= QOrganizerItemPriority::LowPriority)) {
            iPriority.setPriority((QOrganizerItemPriority::Priority) priority);
        } else {
            iPriority.setPriority(QOrganizerItemPriority::UnknownPriority);
        }
        item->saveDetail(&iPriority);
    }
}

void QOrganizerEDSEngine::parseLocation(ECalComponent *comp, QOrganizerItem *item)
{
    gchar *location = e_cal_component_get_location(comp);
    if (location) {
        QOrganizerItemLocation ld = item->detail(QOrganizerItemDetail::TypeLocation);
        ld.setLabel(QString::fromUtf8(location));
        item->saveDetail(&ld);
    }
    g_free(location);
}

void QOrganizerEDSEngine::parseDueDate(ECalClient *client, ECalComponent *comp, QOrganizerItem *item)
{
    ECalComponentDateTime *due = e_cal_component_get_due(comp);
    ICalTime *date = due ? e_cal_component_datetime_get_value(due) : NULL;
    if (date) {
        QOrganizerTodoTime ttr = item->detail(QOrganizerItemDetail::TypeTodoTime);
        QDateTime qdtime = fromIcalTime(client, date, e_cal_component_datetime_get_tzid(due));
        if (qdtime.isValid())
            ttr.setDueDateTime(qdtime);
        if (i_cal_time_is_date(date) != ttr.isAllDay()) {
            ttr.setAllDay(i_cal_time_is_date(date));
        }
        item->saveDetail(&ttr);
    }
    e_cal_component_datetime_free(due);
}

void QOrganizerEDSEngine::parseProgress(ECalComponent *comp, QOrganizerItem *item)
{
    gint percentage = e_cal_component_get_percent_complete(comp);
    if (percentage > 0 && percentage <= 100) {
        QOrganizerTodoProgress tp = item->detail(QOrganizerItemDetail::TypeTodoProgress);
        tp.setPercentageComplete(percentage);
        item->saveDetail(&tp);
    }
}

void QOrganizerEDSEngine::parseStatus(ECalComponent *comp, QOrganizerItem *item)
{
    ICalPropertyStatus status = e_cal_component_get_status(comp);

    QOrganizerTodoProgress tp;
    switch(status) {
        case I_CAL_STATUS_NONE:
            tp.setStatus(QOrganizerTodoProgress::StatusNotStarted);
            break;
        case I_CAL_STATUS_INPROCESS:
            tp.setStatus(QOrganizerTodoProgress::StatusInProgress);
            break;
        case I_CAL_STATUS_COMPLETED:
            tp.setStatus(QOrganizerTodoProgress::StatusComplete);
            break;
        case I_CAL_STATUS_CANCELLED:
        default:
            //TODO: not supported
            break;
    }
    item->saveDetail(&tp);
}

void QOrganizerEDSEngine::parseAttendeeList(ECalComponent *comp, QOrganizerItem *item)
{
    GSList *attendeeList = e_cal_component_get_attendees(comp);
    for (GSList *attendeeIter=attendeeList; attendeeIter != 0; attendeeIter = attendeeIter->next) {
        ECalComponentAttendee *attendee = static_cast<ECalComponentAttendee *>(attendeeIter->data);
        QOrganizerEventAttendee qAttendee;

        qAttendee.setAttendeeId(QString::fromUtf8(e_cal_component_attendee_get_member(attendee)));
        qAttendee.setName(QString::fromUtf8(e_cal_component_attendee_get_cn(attendee)));
        qAttendee.setEmailAddress(QString::fromUtf8(e_cal_component_attendee_get_value(attendee)));

        switch(e_cal_component_attendee_get_role(attendee)) {
        case I_CAL_ROLE_REQPARTICIPANT:
            qAttendee.setParticipationRole(QOrganizerEventAttendee::RoleRequiredParticipant);
            break;
        case I_CAL_ROLE_OPTPARTICIPANT:
            qAttendee.setParticipationRole(QOrganizerEventAttendee::RoleOptionalParticipant);
            break;
        case I_CAL_ROLE_CHAIR:
            qAttendee.setParticipationRole(QOrganizerEventAttendee::RoleChairperson);
            break;
        case I_CAL_ROLE_X:
            qAttendee.setParticipationRole(QOrganizerEventAttendee::RoleHost);
            break;
        case I_CAL_ROLE_NONE:
        default:
            qAttendee.setParticipationRole(QOrganizerEventAttendee::RoleNonParticipant);
            break;
        }

        switch(e_cal_component_attendee_get_partstat(attendee)) {
        case I_CAL_PARTSTAT_ACCEPTED:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusAccepted);
            break;
        case I_CAL_PARTSTAT_DECLINED:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusDeclined);
            break;
        case I_CAL_PARTSTAT_TENTATIVE:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusTentative);
            break;
        case I_CAL_PARTSTAT_DELEGATED:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusDelegated);
            break;
        case I_CAL_PARTSTAT_COMPLETED:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusCompleted);
            break;
        case I_CAL_PARTSTAT_INPROCESS:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusInProcess);
            break;
        case I_CAL_PARTSTAT_NEEDSACTION:
        case I_CAL_PARTSTAT_NONE:
        default:
            qAttendee.setParticipationStatus(QOrganizerEventAttendee::StatusUnknown);
            break;

        }
        item->saveDetail(&qAttendee);
    }
    g_slist_free_full(attendeeList, e_cal_component_attendee_free);
}

void QOrganizerEDSEngine::parseExtendedDetails(ECalComponent *comp, QOrganizerItem *item)
{
    ICalComponent *icalcomp = e_cal_component_get_icalcomponent(comp);
    for (ICalProperty *prop = i_cal_component_get_first_property(icalcomp, I_CAL_X_PROPERTY);
         prop != NULL;
         g_object_unref(prop), prop = i_cal_component_get_next_property (icalcomp, I_CAL_X_PROPERTY)) {

        QOrganizerItemExtendedDetail ex;
        ex.setName(QString::fromUtf8(i_cal_property_get_x_name(prop)));
        ex.setData(QByteArray(i_cal_property_get_x(prop)));
        item->saveDetail(&ex);
    }
}

QOrganizerItem *QOrganizerEDSEngine::parseEvent(ECalClient *client, ECalComponent *comp,
                                                QList<QOrganizerItemDetail::DetailType> detailsHint)
{
    QOrganizerItem *event;
    if (hasRecurrence(comp)) {
        event = new QOrganizerEventOccurrence();
    } else {
        event = new QOrganizerEvent();
    }
    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeEventTime)) {
        parseStartTime(client, comp, event);
        parseEndTime(client, comp, event);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeRecurrence)) {
        parseRecurrence(client, comp, event);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypePriority)) {
        parsePriority(comp, event);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeLocation)) {
        parseLocation(comp, event);
    }
    return event;
}

QOrganizerItem *QOrganizerEDSEngine::parseToDo(ECalClient *client, ECalComponent *comp,
                                               QList<QOrganizerItemDetail::DetailType> detailsHint)
{
    QOrganizerItem *todo;
    if (hasRecurrence(comp)) {
        todo = new QOrganizerTodoOccurrence();
    } else {
        todo = new QOrganizerTodo();
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeTodoTime)) {
        parseTodoStartTime(client, comp, todo);
        parseDueDate(client, comp, todo);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeRecurrence)) {
        parseRecurrence(client, comp, todo);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypePriority)) {
        parsePriority(comp, todo);
    }

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeTodoProgress)) {
        parseProgress(comp, todo);
        parseStatus(comp, todo);
    }

    return todo;
}

QOrganizerItem *QOrganizerEDSEngine::parseJournal(ECalClient *client, ECalComponent *comp,
                                                  QList<QOrganizerItemDetail::DetailType> detailsHint)
{
    QOrganizerJournal *journal = new QOrganizerJournal();

    if (detailsHint.isEmpty() ||
        detailsHint.contains(QOrganizerItemDetail::TypeJournalTime)) {
        ECalComponentDateTime *dt = e_cal_component_get_dtstart(comp);
        ICalTime *date = dt ? e_cal_component_datetime_get_value(dt) : NULL;
        if (date) {
            QDateTime qdtime = fromIcalTime(client, date, e_cal_component_datetime_get_tzid(dt));
            if (qdtime.isValid()) {
              QOrganizerJournalTime jtime;
              jtime.setEntryDateTime(qdtime);
              journal->saveDetail(&jtime);
            }
        }
        e_cal_component_datetime_free(dt);
    }

    return journal;
}

QByteArray QOrganizerEDSEngine::toComponentId(const QByteArray &itemId, QByteArray *rid)
{
    QList<QByteArray> ids = itemId.split('/').last().split('#');
    if (ids.size() == 2) {
        *rid = ids[1];
    }
    return ids[0];
}

ECalComponentId *QOrganizerEDSEngine::ecalComponentId(const QOrganizerItemId &itemId)
{
    QByteArray edsItemId = idToEds(itemId);
    QStringList ids = QString::fromUtf8(edsItemId).split("#");

    QString cId = ids[0];
    QString rId = (ids.size() == 2) ? ids[1] : QString();

    return e_cal_component_id_new(cId.toUtf8().data(), rId.isEmpty() ? NULL : rId.toUtf8().data());
}

void QOrganizerEDSEngine::parseSummary(ECalComponent *comp, QtOrganizer::QOrganizerItem *item)
{
    ECalComponentText *summary = e_cal_component_get_summary(comp);
    if (summary && e_cal_component_text_get_value(summary)) {
        item->setDisplayLabel(QString::fromUtf8(e_cal_component_text_get_value(summary)));
    }
    e_cal_component_text_free(summary);
}

void QOrganizerEDSEngine::parseDescription(ECalComponent *comp, QtOrganizer::QOrganizerItem *item)
{

    GSList *descriptions = e_cal_component_get_descriptions(comp);

    QStringList itemDescription;

    for(GSList *descList = descriptions; descList != 0; descList = descList->next) {
        ECalComponentText *description = static_cast<ECalComponentText*>(descList->data);
        if (description && e_cal_component_text_get_value(description)) {
            itemDescription.append(QString::fromUtf8(e_cal_component_text_get_value(description)));
        }
    }

    item->setDescription(itemDescription.join("\n"));
    g_slist_free_full(descriptions, e_cal_component_text_free);
}

void QOrganizerEDSEngine::parseComments(ECalComponent *comp, QtOrganizer::QOrganizerItem *item)
{
    GSList *comments = e_cal_component_get_comments(comp);
    for(int ci=0, ciMax=g_slist_length(comments); ci < ciMax; ci++) {
        ECalComponentText *txt = static_cast<ECalComponentText*>(g_slist_nth_data(comments, ci));
        item->addComment(QString::fromUtf8(e_cal_component_text_get_value(txt)));
    }
    g_slist_free_full(comments, e_cal_component_text_free);
}

void QOrganizerEDSEngine::parseTags(ECalComponent *comp, QtOrganizer::QOrganizerItem *item)
{
    GSList *categories = e_cal_component_get_categories_list(comp);
    for(GSList *tag=categories; tag != 0; tag = tag->next) {
        item->addTag(QString::fromUtf8(static_cast<gchar*>(tag->data)));
    }
    g_slist_free_full(categories, g_free);
}

QUrl QOrganizerEDSEngine::dencodeAttachment(ECalComponentAlarm *alarm)
{
    QUrl attach;
    GSList *attachments = e_cal_component_alarm_get_attachments(alarm);
    // take the first attachment which is an URL
    for(GSList *attachment=attachments; attachment != 0; attachment = attachment->next) {
        ICalAttach *ica = static_cast<ICalAttach*>(attachment->data);
        if (i_cal_attach_get_is_url(ica)) {
            const gchar *url = i_cal_attach_get_url(ica);
            attach = QUrl(QString::fromUtf8(url));
            break;
        }
    }
    return attach;
}

void QOrganizerEDSEngine::parseVisualReminderAttachment(ECalComponentAlarm *alarm, QOrganizerItemReminder *aDetail)
{
    QUrl attach = dencodeAttachment(alarm);
    if (attach.isValid()) {
        aDetail->setValue(QOrganizerItemVisualReminder::FieldDataUrl, attach);
    }

    ECalComponentText *txt = e_cal_component_alarm_get_description(alarm);
    const gchar *value = txt ? e_cal_component_text_get_value(txt) : NULL;
    aDetail->setValue(QOrganizerItemVisualReminder::FieldMessage, QString::fromUtf8(value));
}

void QOrganizerEDSEngine::parseAudibleReminderAttachment(ECalComponentAlarm *alarm, QOrganizerItemReminder *aDetail)
{
    QUrl attach = dencodeAttachment(alarm);
    if (attach.isValid()) {
        aDetail->setValue(QOrganizerItemAudibleReminder::FieldDataUrl, attach);
    }
}

void QOrganizerEDSEngine::parseReminders(ECalComponent *comp,
                                         QtOrganizer::QOrganizerItem *item,
                                         QList<QtOrganizer::QOrganizerItemDetail::DetailType> detailsHint)
{
    GSList *alarms = e_cal_component_get_alarm_uids(comp);
    for(GSList *a = alarms; a != 0; a = a->next) {
        QOrganizerItemReminder *aDetail = 0;

        QSharedPointer<ECalComponentAlarm> alarm(e_cal_component_get_alarm(comp, static_cast<const gchar*>(a->data)),
                                                 e_cal_component_alarm_free);
        if (!alarm) {
            continue;
        }
        switch(e_cal_component_alarm_get_action(alarm.data()))
        {
            case E_CAL_COMPONENT_ALARM_DISPLAY:
                if (!detailsHint.isEmpty() &&
                    !detailsHint.contains(QOrganizerItemDetail::TypeReminder) &&
                    !detailsHint.contains(QOrganizerItemDetail::TypeVisualReminder)) {
                    continue;
                }
                aDetail = new QOrganizerItemVisualReminder();
                parseVisualReminderAttachment(alarm.data(), aDetail);
                break;
            case E_CAL_COMPONENT_ALARM_AUDIO:
                if (!detailsHint.isEmpty() &&
                    !detailsHint.contains(QOrganizerItemDetail::TypeReminder) &&
                    !detailsHint.contains(QOrganizerItemDetail::TypeAudibleReminder)) {
                    continue;
                }

            // use audio as fallback
            default:
                aDetail = new QOrganizerItemAudibleReminder();
                parseAudibleReminderAttachment(alarm.data(), aDetail);
                break;
        }

        ECalComponentAlarmTrigger *trigger = e_cal_component_alarm_get_trigger(alarm.data());
        int relSecs = 0;
        bool fail = false;
        ECalComponentAlarmTriggerKind kind = e_cal_component_alarm_trigger_get_kind(trigger);
        if (kind == E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_START) {

            ICalDuration *relDuration = e_cal_component_alarm_trigger_get_duration(trigger);
            relSecs = - i_cal_duration_as_int(relDuration);
            if (relSecs < 0) {
                //WORKAROUND: Print warning only once, avoid flood application output
                static bool relativeStartwarningPrinted = false;
                relSecs = 0;
                if (!relativeStartwarningPrinted) {
                    fail = true;
                    relativeStartwarningPrinted = true;
                    qWarning() << "QOrganizer does not support triggers after event start";
                }
            }
        } else if (kind != E_CAL_COMPONENT_ALARM_TRIGGER_NONE) {
            fail = true;
            //WORKAROUND: Print warning only once, avoid flood application output
            static bool warningPrinted = false;
            if (!warningPrinted) {
                qWarning() << "QOrganizer only supports triggers relative to event start.:" << kind;
                warningPrinted = true;
            }
        }

        if (!fail) {
            aDetail->setSecondsBeforeStart(relSecs);
            ECalComponentAlarmRepeat *aRepeat = e_cal_component_alarm_get_repeat(alarm.data());
            if (aRepeat != NULL) {
                int rept = e_cal_component_alarm_repeat_get_repetitions(aRepeat);
                ICalDuration *interval = e_cal_component_alarm_repeat_get_interval(aRepeat);
                aDetail->setRepetition(rept, i_cal_duration_as_int(interval));
            }
            item->saveDetail(aDetail);
        }
        delete aDetail;
    }
    g_slist_free_full(alarms, g_free);
}

void QOrganizerEDSEngine::parseEventsAsync(const QMap<QByteArray, GSList *> &events,
                                           bool isIcalEvents,
                                           QList<QOrganizerItemDetail::DetailType> detailsHint,
                                           QObject *source,
                                           const QByteArray &slot)
{
    QMap<QOrganizerCollectionId, GSList*> request;
    Q_FOREACH(const QByteArray &sourceId, events.keys()) {
        QOrganizerCollectionId collection = d->m_sourceRegistry->collectionId(sourceId);
        if (isIcalEvents) {
            request.insert(collection,
                           g_slist_copy_deep(events.value(sourceId),
                                             (GCopyFunc) i_cal_component_clone, NULL));
        } else {
            request.insert(collection,
                           g_slist_copy_deep(events.value(sourceId),
                                             (GCopyFunc) g_object_ref, NULL));
        }
    }

    // the thread will destroy itself when done
    QOrganizerParseEventThread *thread = new QOrganizerParseEventThread(source, slot);
    thread->start(request, isIcalEvents, detailsHint);
}

QList<QOrganizerItem> QOrganizerEDSEngine::parseEvents(const QOrganizerCollectionId &collectionId, GSList *events, bool isIcalEvents, QList<QOrganizerItemDetail::DetailType> detailsHint)
{
    QList<QOrganizerItem> items;

    EClient *client = m_globalData->m_sourceRegistry->client(collectionId.localId());

    for (GSList *l = events; l; l = l->next) {
        QOrganizerItem *item;
        ECalComponent *comp;
        if (isIcalEvents) {
            ICalComponent *clone = i_cal_component_clone(static_cast<ICalComponent*>(l->data));
            if (clone && i_cal_component_is_valid(clone)) {
                comp = e_cal_component_new_from_icalcomponent(clone);
            } else {
                qWarning() << "Fail to parse event";
                continue;
            }
        } else {
            comp = E_CAL_COMPONENT(l->data);
        }

        //type
        ECalComponentVType vType = e_cal_component_get_vtype(comp);
        switch(vType) {
            case E_CAL_COMPONENT_EVENT:
                item = parseEvent(E_CAL_CLIENT(client), comp, detailsHint);
                break;
            case E_CAL_COMPONENT_TODO:
                item = parseToDo(E_CAL_CLIENT(client), comp, detailsHint);
                break;
            case E_CAL_COMPONENT_JOURNAL:
                item = parseJournal(E_CAL_CLIENT(client), comp, detailsHint);
                break;
            case E_CAL_COMPONENT_FREEBUSY:
                qWarning() << "Component FREEBUSY not supported;";
                continue;
            case E_CAL_COMPONENT_TIMEZONE:
                qWarning() << "Component TIMEZONE not supported;";
            case E_CAL_COMPONENT_NO_TYPE:
                continue;
        }
        // id is mandatory
        parseId(comp, item, collectionId);

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeDescription)) {
            parseDescription(comp, item);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeDisplayLabel)) {
            parseSummary(comp, item);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeComment)) {
            parseComments(comp, item);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeTag)) {
            parseTags(comp, item);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeReminder) ||
            detailsHint.contains(QOrganizerItemDetail::TypeVisualReminder) ||
            detailsHint.contains(QOrganizerItemDetail::TypeAudibleReminder) ||
            detailsHint.contains(QOrganizerItemDetail::TypeEmailReminder)) {
            parseReminders(comp, item, detailsHint);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeEventAttendee)) {
            parseAttendeeList(comp, item);
        }

        if (detailsHint.isEmpty() ||
            detailsHint.contains(QOrganizerItemDetail::TypeExtendedDetail)) {
            parseExtendedDetails(comp, item);
        }

        items << *item;
        delete item;

        if (isIcalEvents) {
            g_object_unref(comp);
        }
    }
    if (client) {
        g_object_unref(client);
    }

    return items;
}

QList<QOrganizerItem> QOrganizerEDSEngine::parseEvents(const QByteArray &sourceId,
                                                       GSList *events,
                                                       bool isIcalEvents,
                                                       QList<QOrganizerItemDetail::DetailType> detailsHint)
{
    QOrganizerCollectionId collection(managerUri(), sourceId);
    return parseEvents(collection, events, isIcalEvents, detailsHint);
}

void QOrganizerEDSEngine::parseStartTime(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerEventTime etr = item.detail(QOrganizerItemDetail::TypeEventTime);
    if (!etr.isEmpty()) {
        QByteArray tzId;
        ICalTime *ict = fromQDateTime(etr.startDateTime(), etr.isAllDay(), &tzId);
        ECalComponentDateTime *dt = e_cal_component_datetime_new_take(ict, g_strdup(tzId.constData()));
        e_cal_component_set_dtstart(comp, dt);
        e_cal_component_datetime_free(dt);
    }
}

void QOrganizerEDSEngine::parseEndTime(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerEventTime etr = item.detail(QOrganizerItemDetail::TypeEventTime);
    if (!etr.isEmpty()) {
        QDateTime eventEndDateTime = etr.endDateTime();
        if (etr.startDateTime() > eventEndDateTime) {
            eventEndDateTime = etr.startDateTime();
        }

        if (etr.isAllDay() &&
            (eventEndDateTime.date() == etr.startDateTime().date())) {
            eventEndDateTime = etr.startDateTime().addDays(1);
        }

        QByteArray tzId;
        ICalTime *ict = fromQDateTime(eventEndDateTime, etr.isAllDay(), &tzId);
        ECalComponentDateTime *dt = e_cal_component_datetime_new_take(ict, g_strdup(tzId.constData()));
        e_cal_component_set_dtend(comp, dt);
        e_cal_component_datetime_free(dt);
    }
}

void QOrganizerEDSEngine::parseTodoStartTime(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerTodoTime etr = item.detail(QOrganizerItemDetail::TypeTodoTime);
    if (!etr.isEmpty() && !etr.startDateTime().isNull()) {
        QByteArray tzId;
        ICalTime *ict = fromQDateTime(etr.startDateTime(), etr.isAllDay(), &tzId);
        ECalComponentDateTime *dt = e_cal_component_datetime_new_take(
                    ict, tzId.isEmpty() ? NULL : g_strdup(tzId.constData()));
        e_cal_component_set_dtstart(comp, dt);
        e_cal_component_datetime_free(dt);
    }
}

void QOrganizerEDSEngine::parseWeekRecurrence(const QOrganizerRecurrenceRule &qRule, ICalRecurrence *rule)
{
    static QMap<Qt::DayOfWeek, ICalRecurrenceWeekday>  daysOfWeekMap({
        { Qt::Monday, I_CAL_MONDAY_WEEKDAY },
        { Qt::Thursday, I_CAL_THURSDAY_WEEKDAY },
        { Qt::Wednesday, I_CAL_WEDNESDAY_WEEKDAY },
        { Qt::Tuesday, I_CAL_TUESDAY_WEEKDAY },
        { Qt::Friday, I_CAL_FRIDAY_WEEKDAY },
        { Qt::Saturday, I_CAL_SATURDAY_WEEKDAY },
        { Qt::Sunday, I_CAL_SUNDAY_WEEKDAY },
    });

    QList<Qt::DayOfWeek> daysOfWeek = qRule.daysOfWeek().toList();
    int c = 0;

    i_cal_recurrence_set_freq(rule, I_CAL_WEEKLY_RECURRENCE);
    for(int d=Qt::Monday; d <= Qt::Sunday; d++) {
        if (daysOfWeek.contains(static_cast<Qt::DayOfWeek>(d))) {
            i_cal_recurrence_set_by_day(rule, c++, daysOfWeekMap[static_cast<Qt::DayOfWeek>(d)]);
        }
    }
    for (int d = c; d < I_CAL_BY_DAY_SIZE; d++) {
        i_cal_recurrence_set_by_day(rule, d, I_CAL_RECURRENCE_ARRAY_MAX);
    }
}

void QOrganizerEDSEngine::parseMonthRecurrence(const QOrganizerRecurrenceRule &qRule, ICalRecurrence *rule)
{
    i_cal_recurrence_set_freq(rule, I_CAL_MONTHLY_RECURRENCE);

    int c = 0;
    Q_FOREACH(int daysOfMonth, qRule.daysOfMonth()) {
        i_cal_recurrence_set_by_month_day(rule, c++, daysOfMonth);
    }
    for (int d = c; d < I_CAL_BY_MONTHDAY_SIZE; d++) {
        i_cal_recurrence_set_by_month_day(rule, d, I_CAL_RECURRENCE_ARRAY_MAX);
    }
}

void QOrganizerEDSEngine::parseYearRecurrence(const QOrganizerRecurrenceRule &qRule, ICalRecurrence *rule)
{
    i_cal_recurrence_set_freq(rule, I_CAL_YEARLY_RECURRENCE);

    QList<int> daysOfYear = qRule.daysOfYear().toList();
    int c = 0;
    for (int d=1; d < I_CAL_BY_YEARDAY_SIZE; d++) {
        if (daysOfYear.contains(d)) {
            i_cal_recurrence_set_by_year_day(rule, c++, d);
        }
    }
    for (int d = c; d < I_CAL_BY_YEARDAY_SIZE; d++) {
        i_cal_recurrence_set_by_year_day(rule, d, I_CAL_RECURRENCE_ARRAY_MAX);
    }

    c = 0;
    QList<QOrganizerRecurrenceRule::Month> monthOfYear = qRule.monthsOfYear().toList();
    for (int d=1; d < I_CAL_BY_MONTH_SIZE; d++) {
        if (monthOfYear.contains(static_cast<QOrganizerRecurrenceRule::Month>(d))) {
            i_cal_recurrence_set_by_month(rule, c++, d);
        }
    }
    for (int d = c; d < I_CAL_BY_MONTH_SIZE; d++) {
        i_cal_recurrence_set_by_month(rule, d, I_CAL_RECURRENCE_ARRAY_MAX);
    }
}



void QOrganizerEDSEngine::parseRecurrence(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerItemRecurrence rec = item.detail(QOrganizerItemDetail::TypeRecurrence);
    if (!rec.isEmpty()) {
        GSList *periodList = NULL;
        Q_FOREACH(const QDate &dt, rec.recurrenceDates()) {
            ICalTime *start = i_cal_time_new_from_timet_with_zone(QDateTime(dt).toTime_t(), FALSE, NULL);
            ECalComponentPeriod *period = e_cal_component_period_new_datetime(start, NULL);
            periodList = g_slist_append(periodList, period);
            //TODO: period.end, period.duration
            g_object_unref(start);
        }
        e_cal_component_set_rdates(comp, periodList);
        g_slist_free_full(periodList, e_cal_component_period_free);

        GSList *exdateList = 0;
        Q_FOREACH(const QDate &dt, rec.exceptionDates()) {
            ICalTime *it = i_cal_time_new_from_timet_with_zone(QDateTime(dt).toTime_t(), FALSE, NULL);
            ECalComponentDateTime *dateTime = e_cal_component_datetime_new_take(it, NULL);
            exdateList = g_slist_append(exdateList, dateTime);
        }
        e_cal_component_set_exdates(comp, exdateList);
        g_slist_free_full(exdateList, e_cal_component_datetime_free);

        GSList *ruleList = 0;
        Q_FOREACH(const QOrganizerRecurrenceRule &qRule, rec.recurrenceRules()) {
            ICalRecurrence *rule = i_cal_recurrence_new();
            switch(qRule.frequency()) {
                case QOrganizerRecurrenceRule::Daily:
                    i_cal_recurrence_set_freq(rule, I_CAL_DAILY_RECURRENCE);
                    break;
                case QOrganizerRecurrenceRule::Weekly:
                    parseWeekRecurrence(qRule, rule);
                    break;
                case QOrganizerRecurrenceRule::Monthly:
                    parseMonthRecurrence(qRule, rule);
                    break;
                case QOrganizerRecurrenceRule::Yearly:
                    parseYearRecurrence(qRule, rule);
                    break;
                case QOrganizerRecurrenceRule::Invalid:
                    i_cal_recurrence_set_freq(rule, I_CAL_NO_RECURRENCE);
                    break;
            }

            switch (qRule.limitType()) {
            case QOrganizerRecurrenceRule::DateLimit:
                if (qRule.limitDate().isValid()) {
                    ICalTime *until = i_cal_time_new_from_day_of_year(qRule.limitDate().dayOfYear(),
                                                                      qRule.limitDate().year());
                    i_cal_recurrence_set_until(rule, until);
                    g_object_unref(until);
                }
                break;
            case QOrganizerRecurrenceRule::CountLimit:
                if (qRule.limitCount() > 0) {
                    i_cal_recurrence_set_count(rule, qRule.limitCount());
                }
                break;
            case QOrganizerRecurrenceRule::NoLimit:
            default:
                i_cal_recurrence_set_count(rule, 0);
            }

            QSet<int> positions = qRule.positions();
            for (int d=1; d < I_CAL_BY_SETPOS_SIZE; d++) {
                if (positions.contains(d)) {
                    i_cal_recurrence_set_by_set_pos(rule, d, d);
                } else {
                    i_cal_recurrence_set_by_set_pos(rule, d, I_CAL_RECURRENCE_ARRAY_MAX);
                }
            }

            i_cal_recurrence_set_interval(rule, qRule.interval());
            ruleList = g_slist_append(ruleList, rule);
        }
        e_cal_component_set_rrules(comp, ruleList);
        g_slist_free_full(ruleList, g_object_unref);
    }
}

void QOrganizerEDSEngine::parsePriority(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerItemPriority priority = item.detail(QOrganizerItemDetail::TypePriority);
    if (!priority.isEmpty()) {
        e_cal_component_set_priority(comp, (gint) priority.priority());
    }
}

void QOrganizerEDSEngine::parseLocation(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerItemLocation ld = item.detail(QOrganizerItemDetail::TypeLocation);
    if (!ld.isEmpty()) {
        e_cal_component_set_location(comp, ld.label().toUtf8().data());
    }
}

void QOrganizerEDSEngine::parseDueDate(const QtOrganizer::QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerTodoTime ttr = item.detail(QOrganizerItemDetail::TypeTodoTime);
    if (!ttr.isEmpty() && !ttr.dueDateTime().isNull()) {
        QDateTime dueDateTime = ttr.dueDateTime();
        if (ttr.startDateTime() > dueDateTime) {
            dueDateTime = ttr.startDateTime();
        }

        if (ttr.isAllDay() &&
            (dueDateTime.date() == ttr.startDateTime().date())) {
            dueDateTime = ttr.startDateTime().addDays(1);
        }

        QByteArray tzId;
        ICalTime *ic = fromQDateTime(dueDateTime, ttr.isAllDay(), &tzId);
        gchar *tzid = tzId.isEmpty() ? NULL : g_strdup(tzId.constData());
        ECalComponentDateTime *dt = e_cal_component_datetime_new_take(ic, tzid);
        e_cal_component_set_due(comp, dt);
        e_cal_component_datetime_free(dt);
    }
}

void QOrganizerEDSEngine::parseProgress(const QtOrganizer::QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerTodoProgress tp = item.detail(QOrganizerItemDetail::TypeTodoProgress);
    if (!tp.isEmpty() && (tp.percentageComplete() > 0)) {
        e_cal_component_set_percent_complete(comp, tp.percentageComplete());
    }
}

void QOrganizerEDSEngine::parseStatus(const QtOrganizer::QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerTodoProgress tp = item.detail(QOrganizerItemDetail::TypeTodoProgress);
    if (!tp.isEmpty()) {
        switch(tp.status()) {
            case QOrganizerTodoProgress::StatusNotStarted:
                e_cal_component_set_status(comp, I_CAL_STATUS_NONE);
                break;
            case QOrganizerTodoProgress::StatusInProgress:
                e_cal_component_set_status(comp, I_CAL_STATUS_INPROCESS);
                break;
            case QOrganizerTodoProgress::StatusComplete:
                e_cal_component_set_status(comp, I_CAL_STATUS_COMPLETED);
                break;
            default:
                e_cal_component_set_status(comp, I_CAL_STATUS_CANCELLED);
                break;
        }
    }
}

void QOrganizerEDSEngine::parseAttendeeList(const QOrganizerItem &item, ECalComponent *comp)
{
    GSList *attendeeList = 0;

    Q_FOREACH(const QOrganizerEventAttendee &attendee, item.details(QOrganizerItemDetail::TypeEventAttendee)) {
        ECalComponentAttendee *calAttendee = e_cal_component_attendee_new();

        e_cal_component_attendee_set_member(calAttendee,
                                            attendee.attendeeId().toUtf8().constData());
        e_cal_component_attendee_set_cn(calAttendee,
                                        attendee.name().toUtf8().constData());
        e_cal_component_attendee_set_value(calAttendee,
                                           attendee.emailAddress().toUtf8().constData());

        ICalParameterRole role;
        switch(attendee.participationRole()) {
        case QOrganizerEventAttendee::RoleRequiredParticipant:
            role = I_CAL_ROLE_REQPARTICIPANT;
            break;
        case QOrganizerEventAttendee::RoleOptionalParticipant:
            role = I_CAL_ROLE_OPTPARTICIPANT;
            break;
        case QOrganizerEventAttendee::RoleChairperson:
            role = I_CAL_ROLE_CHAIR;
            break;
        case QOrganizerEventAttendee::RoleHost:
            role = I_CAL_ROLE_X;
            break;
        default:
            role = I_CAL_ROLE_NONE;
        }
        e_cal_component_attendee_set_role(calAttendee, role);

        ICalParameterPartstat status;
        switch(attendee.participationStatus()) {
        case QOrganizerEventAttendee::StatusAccepted:
            status = I_CAL_PARTSTAT_ACCEPTED;
            break;
        case QOrganizerEventAttendee::StatusDeclined:
            status = I_CAL_PARTSTAT_DECLINED;
            break;
        case QOrganizerEventAttendee::StatusTentative:
            status = I_CAL_PARTSTAT_TENTATIVE;
            break;
        case QOrganizerEventAttendee::StatusDelegated:
            status = I_CAL_PARTSTAT_DELEGATED;
            break;
        case QOrganizerEventAttendee::StatusInProcess:
            status = I_CAL_PARTSTAT_INPROCESS;
            break;
        case QOrganizerEventAttendee::StatusCompleted:
            status = I_CAL_PARTSTAT_COMPLETED;
            break;
        case QOrganizerEventAttendee::StatusUnknown:
        default:
            status = I_CAL_PARTSTAT_NONE;
            break;
        }
        e_cal_component_attendee_set_partstat(calAttendee, status);
        attendeeList = g_slist_append(attendeeList, calAttendee);
    }
    e_cal_component_set_attendees(comp, attendeeList);
    g_slist_free_full(attendeeList, e_cal_component_attendee_free);
}

void QOrganizerEDSEngine::parseExtendedDetails(const QOrganizerItem &item, ECalComponent *comp)
{
    ICalComponent *icalcomp = e_cal_component_get_icalcomponent(comp);
    Q_FOREACH(const QOrganizerItemExtendedDetail &ex, item.details(QOrganizerItemDetail::TypeExtendedDetail)) {
        // We only support QByteArray.
        // We could use QStream serialization but it will make it impossible to read it from glib side, for example indicators.
        QByteArray data = ex.data().toByteArray();
        if (data.isEmpty()) {
            qWarning() << "Invalid value for property" << ex.name()
                       <<". EDS only supports QByteArray values for extended properties";
            continue;
        }

        ICalProperty *xProp = i_cal_property_new_x(data.constData());
        i_cal_property_set_x_name(xProp, ex.name().toUtf8().constData());
        i_cal_component_take_property(icalcomp, xProp);
    }
}

bool QOrganizerEDSEngine::hasRecurrence(ECalComponent *comp)
{
    char *rid = e_cal_component_get_recurid_as_string(comp);
    bool result = (rid && strcmp(rid, "0"));
    if (rid) {
        free(rid);
    }
    return result;
}

void QOrganizerEDSEngine::parseId(ECalComponent *comp,
                                  QOrganizerItem *item,
                                  const QOrganizerCollectionId &collectionId)
{
    ECalComponentId *id = e_cal_component_get_id(comp);

    if (collectionId.isNull()) {
        qWarning() << "Parse Id with null collection";
        return;
    }

    QByteArray iId(e_cal_component_id_get_uid(id));
    QByteArray rId(e_cal_component_id_get_rid(id));
    if (!rId.isEmpty()) {
        iId += "#" + rId;
    }

    QByteArray itemGuid =
        iId.contains(':') ? iId.mid(iId.lastIndexOf(':') + 1) : iId;
    QOrganizerItemId itemId = idFromEds(collectionId, itemGuid);
    item->setId(itemId);
    item->setGuid(QString::fromUtf8(itemId.localId()));

    if (!rId.isEmpty()) {
        QOrganizerItemParent itemParent = item->detail(QOrganizerItemDetail::TypeParent);
        QOrganizerItemId parentId(idFromEds(collectionId, QByteArray(e_cal_component_id_get_uid(id))));
        itemParent.setParentId(parentId);
        item->saveDetail(&itemParent);
    }

    item->setCollectionId(collectionId);
    e_cal_component_id_free(id);
}

ECalComponent *QOrganizerEDSEngine::createDefaultComponent(ECalClient *client,
                                                           ICalComponentKind iKind,
                                                           ECalComponentVType eType)
{
    ECalComponent *comp;
    ICalComponent *icalcomp = 0;

    if (client && !e_cal_client_get_default_object_sync(client, &icalcomp, NULL, NULL)) {
        icalcomp = i_cal_component_new(iKind);
    }

    comp = e_cal_component_new();
    if (icalcomp && !e_cal_component_set_icalcomponent(comp, icalcomp)) {
        g_object_unref(icalcomp);
    }

    e_cal_component_set_new_vtype(comp, eType);

    return comp;
}

ECalComponent *QOrganizerEDSEngine::parseEventItem(ECalClient *client, const QOrganizerItem &item)
{
    ECalComponent *comp = createDefaultComponent(client, I_CAL_VEVENT_COMPONENT, E_CAL_COMPONENT_EVENT);

    parseStartTime(item, comp);
    parseEndTime(item, comp);
    parseRecurrence(item, comp);
    parsePriority(item, comp);
    parseLocation(item, comp);
    return comp;

}

ECalComponent *QOrganizerEDSEngine::parseTodoItem(ECalClient *client, const QOrganizerItem &item)
{
    ECalComponent *comp = createDefaultComponent(client, I_CAL_VTODO_COMPONENT, E_CAL_COMPONENT_TODO);

    parseTodoStartTime(item, comp);
    parseDueDate(item, comp);
    parseRecurrence(item, comp);
    parsePriority(item, comp);
    parseProgress(item, comp);
    parseStatus(item, comp);

    return comp;
}

ECalComponent *QOrganizerEDSEngine::parseJournalItem(ECalClient *client, const QOrganizerItem &item)
{
    ECalComponent *comp = createDefaultComponent(client, I_CAL_VJOURNAL_COMPONENT, E_CAL_COMPONENT_JOURNAL);

    QOrganizerJournalTime jtime = item.detail(QOrganizerItemDetail::TypeJournalTime);
    if (!jtime.isEmpty()) {
        QByteArray tzId;
        ICalTime *ic = fromQDateTime(jtime.entryDateTime(), false, &tzId);
        gchar *tzid = tzId.isEmpty() ? NULL : g_strdup(tzId.constData());
        ECalComponentDateTime *dt = e_cal_component_datetime_new_take(ic, tzid);
        e_cal_component_set_dtstart(comp, dt);
        e_cal_component_datetime_free(dt);
    }

    return comp;
}

void QOrganizerEDSEngine::parseSummary(const QOrganizerItem &item, ECalComponent *comp)
{
    //summary
    if (!item.displayLabel().isEmpty()) {
        QByteArray str = item.displayLabel().toUtf8();
        ECalComponentText *txt = e_cal_component_text_new(str.constData(), NULL);
        e_cal_component_set_summary(comp, txt);
        e_cal_component_text_free(txt);
    }
}

void QOrganizerEDSEngine::parseDescription(const QOrganizerItem &item, ECalComponent *comp)
{
    //description
    if (!item.description().isEmpty()) {
        GSList *descriptions = 0;

        QByteArray str = item.description().toUtf8();
        ECalComponentText *txt = e_cal_component_text_new(str.constData(), NULL);
        descriptions = g_slist_append(descriptions, txt);

        e_cal_component_set_descriptions(comp, descriptions);
        g_slist_free_full(descriptions, e_cal_component_text_free);
    }
}

void QOrganizerEDSEngine::parseComments(const QOrganizerItem &item, ECalComponent *comp)
{
    //comments
    GSList *comments = 0;
    QList<QByteArray> commentList;

    Q_FOREACH(const QString &comment, item.comments()) {
        QByteArray str = comment.toUtf8();
        ECalComponentText *txt = e_cal_component_text_new(str.constData(), NULL);
        comments = g_slist_append(comments, txt);
        // keep str alive until the property gets updated
        commentList << str;
    }

    if (comments) {
        e_cal_component_set_comments(comp, comments);
        g_slist_free_full(comments, e_cal_component_text_free);
    }
}

void QOrganizerEDSEngine::parseTags(const QOrganizerItem &item, ECalComponent *comp)
{
    //tags
    GSList *categories = 0;
    QList<QByteArray> tagList;

    Q_FOREACH(const QString &tag, item.tags()) {
        QByteArray str = tag.toUtf8();
        categories = g_slist_append(categories, str.data());
        // keep str alive until the property gets updated
        tagList << str;
    }

    if (categories) {
        e_cal_component_set_categories_list(comp, categories);
        g_slist_free(categories);
    }
}

void QOrganizerEDSEngine::encodeAttachment(const QUrl &url, ECalComponentAlarm *alarm)
{
    if (!url.isEmpty()) {
        ICalAttach *attach = i_cal_attach_new_from_url(url.toString().toUtf8());
        GSList *attachments = g_slist_append(NULL, attach);
        e_cal_component_alarm_take_attachments(alarm, attachments);
    }
}

void QOrganizerEDSEngine::parseVisualReminderAttachment(const QOrganizerItemDetail &detail, ECalComponentAlarm *alarm)
{
    QByteArray str = detail.value(QOrganizerItemVisualReminder::FieldMessage).toString().toUtf8();
    if (!str.isEmpty()) {
        ECalComponentText *txt = e_cal_component_text_new(str.constData(), NULL);
        e_cal_component_alarm_set_description(alarm, txt);
        e_cal_component_text_free(txt);
    }
    encodeAttachment(detail.value(QOrganizerItemVisualReminder::FieldDataUrl).toUrl(), alarm);
}

void QOrganizerEDSEngine::parseAudibleReminderAttachment(const QOrganizerItemDetail &detail, ECalComponentAlarm *alarm)
{
    encodeAttachment(detail.value(QOrganizerItemAudibleReminder::FieldDataUrl).toUrl(), alarm);
}

void QOrganizerEDSEngine::parseReminders(const QOrganizerItem &item, ECalComponent *comp)
{
    //reminders
    QList<QOrganizerItemDetail> reminders = item.details(QOrganizerItemDetail::TypeAudibleReminder);
    reminders += item.details(QOrganizerItemDetail::TypeVisualReminder);

    Q_FOREACH(const QOrganizerItemDetail &detail, reminders) {
        const QOrganizerItemReminder *reminder = static_cast<const QOrganizerItemReminder*>(&detail);
        ECalComponentAlarm *alarm = e_cal_component_alarm_new();
        switch(reminder->type())
        {
            case QOrganizerItemReminder::TypeVisualReminder:
                e_cal_component_alarm_set_action(alarm, E_CAL_COMPONENT_ALARM_DISPLAY);
                parseVisualReminderAttachment(detail, alarm);
                break;
            case QOrganizerItemReminder::TypeAudibleReminder:
            default:
                // use audio as fallback
                e_cal_component_alarm_set_action(alarm, E_CAL_COMPONENT_ALARM_AUDIO);
                parseAudibleReminderAttachment(detail, alarm);
                break;
        }

        ICalDuration *tDuration = i_cal_duration_new_from_int(- reminder->secondsBeforeStart());
        ECalComponentAlarmTrigger *trigger = e_cal_component_alarm_trigger_new_relative(E_CAL_COMPONENT_ALARM_TRIGGER_RELATIVE_START, tDuration);
        g_object_unref(tDuration);
        e_cal_component_alarm_set_trigger(alarm, trigger);

        // TODO: check if this is really necessary
        int repetitions = reminder->repetitionCount(); //qMax(reminder->repetitionCount(), 1);
        ECalComponentAlarmRepeat *aRepeat = e_cal_component_alarm_repeat_new_seconds(repetitions,
                                                                                     reminder->repetitionDelay());
        e_cal_component_alarm_set_repeat(alarm, aRepeat);

        e_cal_component_add_alarm(comp, alarm);
        e_cal_component_alarm_free(alarm);
    }
}

QOrganizerItemId
QOrganizerEDSEngine::idFromEds(const QOrganizerCollectionId &collectionId,
                               const gchar *uid)
{
    return QOrganizerItemId(collectionId.managerUri(),
                            collectionId.localId() + '/' + QByteArray(uid));
}

QByteArray QOrganizerEDSEngine::idToEds(const QOrganizerItemId &itemId,
                                        QByteArray *sourceId)
{
    QList<QByteArray> ids = itemId.localId().split('/');
    if (ids.length() == 2) {
        if (sourceId) *sourceId = ids[0];
        return ids[1];
    } else {
        if (sourceId) *sourceId = QByteArray();
        return QByteArray();
    }
}

GSList *QOrganizerEDSEngine::parseItems(ECalClient *client,
                                        QList<QOrganizerItem> items,
                                        bool *hasRecurrence)
{
    GSList *comps = 0;

    Q_FOREACH(const QOrganizerItem &item, items) {
        ECalComponent *comp = 0;

        *hasRecurrence = ((item.type() == QOrganizerItemType::TypeTodoOccurrence) ||
                          (item.type() == QOrganizerItemType::TypeEventOccurrence));

        switch(item.type()) {
            case QOrganizerItemType::TypeEvent:
            case QOrganizerItemType::TypeEventOccurrence:
                comp = parseEventItem(client, item);
                break;
            case QOrganizerItemType::TypeTodo:
            case QOrganizerItemType::TypeTodoOccurrence:
                comp = parseTodoItem(client, item);
                break;
            case QOrganizerItemType::TypeJournal:
                comp = parseJournalItem(client, item);
                break;
            case QOrganizerItemType::TypeNote:
                qWarning() << "Component TypeNote not supported;";
            case QOrganizerItemType::TypeUndefined:
            default:
                continue;
        }
        parseId(item, comp);
        parseSummary(item, comp);
        parseDescription(item, comp);
        parseComments(item, comp);
        parseTags(item, comp);
        parseReminders(item, comp);
        parseAttendeeList(item, comp);
        parseExtendedDetails(item, comp);

        if (!item.id().isNull()) {
            e_cal_component_commit_sequence(comp);
        } else {
            e_cal_component_abort_sequence(comp);
        }

        comps = g_slist_append(comps,
                               i_cal_component_clone(e_cal_component_get_icalcomponent(comp)));

        g_object_unref(comp);
    }

    return comps;
}

void QOrganizerEDSEngine::parseId(const QOrganizerItem &item, ECalComponent *comp)
{
    QOrganizerItemId itemId = item.id();
    if (!itemId.isNull()) {
        QByteArray fullItemId = idToEds(itemId);
        QByteArray rId;
        QByteArray cId = toComponentId(fullItemId, &rId);

        e_cal_component_set_uid(comp, cId.data());

        if (!rId.isEmpty()) {
            // use component tz on recurrence id
            ECalComponentDateTime *dt = e_cal_component_get_dtstart(comp);
            e_cal_component_datetime_take_value(dt, i_cal_time_new_from_string(rId.data()));
            ECalComponentRange *recur_id = e_cal_component_range_new_take(E_CAL_COMPONENT_RANGE_SINGLE, dt);
            e_cal_component_set_recurid(comp, recur_id);
            e_cal_component_range_free(recur_id);
        }
    }
}
