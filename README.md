# QtOrganizer engine for EDS calendar

Qt Organizer provides clients with the ability to access calendar,
schedule, and personal data in a platform-independent and
datastore-agnostic manner. This is achieved by defining generic personal
information data abstractions which can sufficiently describe calendar
and scheduling data stored in the native calendaring system of a
platform. Through the plugin architecture, Qt Organizer can be used as a
front-end API for any calendaring system, such as an online calendar.

This package provides a QOrganizerEngine implementation using Evolution
Data Server as storage backend.
